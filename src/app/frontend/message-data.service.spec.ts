import {MessageDataService} from './message-data.service';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {fakeAsync, tick} from '@angular/core/testing';

describe('MessageDataService', () => {
  const httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'put']);
  let messageDataService: MessageDataService;
  beforeEach(() => {
    messageDataService = new MessageDataService(httpSpy as HttpClient);
  });

  it('should be created', () => {
    expect(messageDataService).toBeDefined();
  });

  it('should extract the messages out of the response', fakeAsync(() => {
    const body = {
      messages:
        [{
          message_date: '2019-02-17T12:49:03+00:00',
          message_id: 71, read_status: false, text: 'Your task \"x\" has been commented by Moritz Leitner.'
        },
          {
            message_date: '2019-02-17T12:47:34+00:00',
            message_id: 70,
            read_status: false,
            text: 'Your task \"x\" has been signed by Moritz Leitner.'
          }]
    };

    httpSpy.get.and.returnValue(of(body));
    messageDataService.getMessages(1, 20).subscribe(
      value => {
        expect(value).toBe(body.messages);
      },
      error => fail());
    tick();
  }));

  it('should return true on setMessagesRead if response status is 200', fakeAsync(() => {
    httpSpy.put.and.returnValue(of(new HttpResponse({status: 200})));
    expect(messageDataService.setMessagesRead(1).subscribe(value => {
      expect(value).toBe(true);
    }, error => fail()));
  }));

  it('should update the readCount on the Subject, when sendNumberMessagesRead is called', fakeAsync(() => {
    messageDataService.getMessagesToReadCount().subscribe(value => {
      expect(value).toBe('5');
    }, error => fail());
    messageDataService.sendNumberMessagesToRead('5');
    tick();
  }));

  it('should clear the Subject, when clear is called', fakeAsync(() => {
    messageDataService.sendNumberMessagesToRead('5');
    messageDataService.getMessagesToReadCount().subscribe(value => fail(), error => fail());
    messageDataService.clear();
    tick();
  }));
});
