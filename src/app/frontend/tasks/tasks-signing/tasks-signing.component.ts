import {Component} from '@angular/core';
import {TaskDataService} from '../../../shared/task-data.service';
import {CacheMapService} from '../../../shared/cache-map.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskOverviewComponent} from '../task-overview/task-overview.component';
import {TaskTypes} from '../../../models/task-types.enum';

@Component({
  selector: 'app-tasks-signing',
  templateUrl: './tasks-signing.component.html',
  styleUrls: ['./tasks-signing.component.css']
})
export class TasksSigningComponent extends TaskOverviewComponent {

  constructor(protected taskDataService: TaskDataService, protected cache: CacheMapService, protected router: Router, protected activatedRoute: ActivatedRoute) {
    super(taskDataService, cache, router, activatedRoute);
    this.taskType = TaskTypes.TOSIGN;
  }

  transformDate(date: string): string {
    return new Date(date).toLocaleString();
  }

}
