import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {RouteGenerator} from '../shared/route-generator';

@Injectable({
  providedIn: 'root'
})
export class FileDataService {

  constructor(private httpClient: HttpClient) {
  }

  getFilesByUrl(id: number) {
    return this.httpClient.get(RouteGenerator.getTasksFilesRoute(id), {observe: 'response'})
      .pipe(map(response => {
        return RouteGenerator.getDownloadRoute(response.body['link']);
      }));
  }
}
