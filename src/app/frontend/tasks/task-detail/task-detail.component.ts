import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Task} from '../../../models/task';
import {FieldTypes} from '../../../models/field-types.enum';
import {TaskDataService} from '../../../shared/task-data.service';
import {SignatureTypes} from '../../../models/signature-types.enum';
import {UserDataService} from '../../../shared/user-data.service';
import {TaskEvent} from '../../../models/task-event.enum';
import {FileDataService} from '../../file-data.service';
import {combineLatest, EMPTY, Observable} from 'rxjs';
import {Md5} from 'ts-md5/dist/md5';
import {User} from '../../../models/user';
import {CacheMapService} from '../../../shared/cache-map.service';
import {catchError, map} from 'rxjs/operators';
import {AlertMessage} from '../../../alert/alert-messages';
import {DE_Confirm, EN_Confirm} from '../../../models/confirm.language';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {

  message: AlertMessage;
  signBox = false;
  commentsPerPage = 10;
  commentPage = 1;
  comments: Observable<Comment[]>;
  allCommentsLoaded = null;
  comment = '';
  task: Task;
  currentUser: User;
  currentUserHash: string;
  rejectable: boolean;
  cantSign = null;
  fieldTypes = FieldTypes;
  signatureTypes = SignatureTypes;
  successfulRejected = null;
  isSigned = null;

  constructor(private route: ActivatedRoute, private taskDataService: TaskDataService,
              private userDataService: UserDataService, private fileDataService: FileDataService,
              private cache: CacheMapService, private router: Router) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: { task: Task }) => {
      this.task = data.task;
      this.comments = this.taskDataService.getComments(this.task.task_id, this.commentsPerPage, this.commentPage).pipe(map(comments => {
        this.allCommentsLoaded = comments.length < this.commentsPerPage;
        return comments;
      }));
      this.userDataService.getCurrentUser().then(user => {
        this.currentUser = user;
        this.currentUserHash = Md5.hashStr(user.email_address.toLowerCase()).toString();
        this.rejectable = this.task.creator.user_id === this.currentUser.user_id && this.task.task_status === 'pending';
      });
    });
  }

  getLastChange() {
    const accesses = this.task.accesses;
    let maxDate = new Date(accesses[accesses.length - 1].event_date);
    for (let i = accesses.length - 1; i >= 0; i--) {
      const temp = new Date(accesses[i].event_date);
      if (maxDate.getTime() < temp.getTime()) {
        maxDate = temp;
      }
    }
    return maxDate.getTime() > new Date(this.task.creation_date).getTime() ? maxDate : this.task.creation_date;
  }

  getFiles() {
    const fields = this.task.tasks_fields;
    return fields.filter(x => x['field']['field_type'] === FieldTypes.FILE.toString());
  }

  getFieldsWithoutFiles() {
    return this.task.tasks_fields.filter(x => x['field']['field_type'] !== FieldTypes.FILE.toString());
  }

  convertToBoolean(value) {
    return value === 'True';
  }

  reject(confirm = window.confirm) {
    const lang = localStorage.getItem('localeId') === 'en' ? EN_Confirm : DE_Confirm;
    if (confirm(lang.REJECT_TASK)) {
      this.taskDataService.sendEvent(this.task.task_id, TaskEvent.DENIED).subscribe(x => {
        this.reload(null, true);
      }, error => {
        this.message = AlertMessage.NOT_SUCCESSFUL_REJECTED;
      });
    }
  }

  sign() {
    this.taskDataService.sendEvent(this.task.task_id, TaskEvent.SIGNED).subscribe(x => {
      this.reload(true);
    }, error => {
      if (error.status === 400) {
        this.cantSign = true;
      } else {
        this.message = AlertMessage.SOMETHING_WENT_WRONG;
      }
    });
  }

  signCheckbox() {
    this.signBox = true;
    this.sign();
  }

  reload(signed = null, rejected = null) {
    this.cache.flush('tasks');
    this.taskDataService.getTaskDetails(this.task.task_id).subscribe(result => {
      this.message = AlertMessage.NOT_SET;
      this.successfulRejected = rejected;
      this.isSigned = signed;
      this.cantSign = null;
      this.task = result;
      this.rejectable = this.task.creator.user_id === this.currentUser.user_id && this.task.task_status === 'pending';
    }, error => {
      this.message = AlertMessage.CAN_NOT_RELOAD;
    });
    this.comments = this.taskDataService.getComments(this.task.task_id, this.commentsPerPage, this.commentPage).pipe(catchError(err => {
      this.message = AlertMessage.SOMETHING_WENT_WRONG;
      return EMPTY;
    }));
  }

  download() {
    this.fileDataService.getFilesByUrl(this.task.task_id).subscribe(url => {
      window.open(url, '_blank');
      if (this.task.signature_type === SignatureTypes.DOWNLOAD_BUTTON || this.task.signature_type === SignatureTypes.DOWNLOAD_CHECKBOX) {
        this.reload();
      }
    }, err => {
      this.message = AlertMessage.SOMETHING_WENT_WRONG;
    });
  }

  getUserHash(user: User) {
    return Md5.hashStr(user.email_address.toLowerCase()).toString();
  }

  addComment() {
    this.taskDataService.addComment(this.task.task_id, this.comment).subscribe(x => {
      this.reload();
      this.comment = '';
    });
  }

  deleteTask(confirm = window.confirm) {
    const lang = localStorage.getItem('localeId') === 'en' ? EN_Confirm : DE_Confirm;
    if (confirm(lang.WITHDRAW_TASK)) {
      this.taskDataService.deleteTask(this.task.task_id).subscribe(result => {
        this.router.navigateByUrl('/tasks/owned');
      }, err => {
        this.message = AlertMessage.SOMETHING_WENT_WRONG;
      });
    }
  }

  nextCommentPage() {
    this.commentPage += 1;
    this.comments = combineLatest(this.comments, this.taskDataService.getComments(this.task.task_id, this.commentsPerPage, this.commentPage))
      .pipe(map(result => {
        this.allCommentsLoaded = result[1].length < this.commentsPerPage;
        return result[0].concat(result[1]);
      }), catchError(err => {
        this.message = AlertMessage.SOMETHING_WENT_WRONG;
        return EMPTY;
      }));
  }

  transformDate(date: string): string {
    return new Date(date).toLocaleString();
  }
}
