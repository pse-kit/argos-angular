import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {TaskDataService} from 'src/app/shared/task-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EndpointDataService} from 'src/app/shared/endpoint-data.service';
import {UserDataService} from 'src/app/shared/user-data.service';
import {forkJoin, Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {HttpEvent, HttpEventType} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {Task} from 'src/app/models/task';
import {Endpoint} from 'src/app/models/endpoint';
import {FieldTypes} from 'src/app/models/field-types.enum';
import {CanComponentDeactivate} from '../../../shared/can-deactivate.guard';
import {User} from 'src/app/models/user';
import {AlertMessage} from '../../../alert/alert-messages';
import {DE_Confirm, EN_Confirm} from '../../../models/confirm.language';

/**
 * Component for creating a task.
 */
@Component({
  selector: 'app-task-creation',
  templateUrl: './task-creation.component.html',
  styleUrls: ['./task-creation.component.css']
})
export class TaskCreationComponent implements OnInit, CanComponentDeactivate {
  requiredFile = true;
  searchInput;
  currentUser: User;
  endpoint: Endpoint = null;
  taskCreationForm: FormGroup;
  fieldsAddedToForm: boolean;
  fileSubmitted: boolean;
  percentDone: number;
  additionalAccesses: any[] = [];
  fieldTypes = FieldTypes;
  formFiles: File[];
  message: AlertMessage;

  constructor(private taskData: TaskDataService,
              private endpointData: EndpointDataService,
              private userData: UserDataService,
              private route: ActivatedRoute,
              @Inject(LOCALE_ID) private locale: string,
              private router: Router) {
  }

  /**
   * Get the form fields set in the taskCreationForm.
   */
  get fields(): FormArray {
    return this.taskCreationForm.get('fields') as FormArray;
  }

  ngOnInit() {
    this.fieldsAddedToForm = false;
    this.resetComponent();
    this.route.params.subscribe(params => {
      this.endpointData.getEndpoint(+params.id).subscribe(result => {
        this.endpoint = result;
        this.resetComponent();
      }, error => {
        // invalid endpoint id provided
        this.message = AlertMessage.NO_MATCH_WITH_ENDPOINT_ID;
        setTimeout(() => {
          this.router.navigateByUrl('/tasks/owned)');
        }, 5000);
      });
    });
  }

  /**
   * Search for user and groups matching the parameter text.
   * @param text search string.
   */
  search = (text: Observable<string>) =>
    text.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        forkJoin(this.userData.userSuggestions(term), this.userData.groupSuggestions(term))
      ), map(result => result[0].concat(result[1]).filter(user => user.user_id !== this.currentUser.user_id).slice(0, 10)));

  /**
   * Format the search result to show only the name.
   */
  formatter = (result: any) => result.name;

  /**
   * Add endpoint fields to taskCreationForm.
   */
  addFieldsToForm() {
    for (let field of this.endpoint.fields) {
      let value = null;
      if (field.field_type.toString() === FieldTypes.CHECKBOX.toString()) {
        value = false;
      }

      let valueControl;
      if (field.required_status) {
        if (field.field_type.toString() === FieldTypes.FILE.toString()) {
          valueControl = new FormControl(value);
          this.requiredFile = false;
        } else {
          valueControl = new FormControl(value, Validators.required);
        }
      } else {
        valueControl = new FormControl(value);
      }

      (<FormArray>this.taskCreationForm.controls.fields).push(
        new FormGroup({
          'field_id': new FormControl(field.field_id),
          'field_type': new FormControl(field.field_type),
          'name': new FormControl(field.name),
          'value': valueControl,
          'required_status': new FormControl(field.required_status)
        })
      );
    }
    this.fieldsAddedToForm = true;
  }

  /**
   * Add an additional signature.
   */
  addSignature(sig: any) {
    const isUser = sig.item.user_id !== undefined;
    this.additionalAccesses.push({signee: sig.item, isUser: isUser});
    setTimeout(() => this.searchInput = '', 1);
  }

  /**
   * Remove an additional signature.
   * @param index index of signature in FormArray
   */
  removeAdditionalAccess(index: number) {
    this.additionalAccesses.splice(index, 1);
  }

  /**
   * Submit the task with the inputs made by the user.
   */
  onSubmit() {
    // scroll on top of window
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20);
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 5);

    const task: Task = {
      endpoint_id: this.endpoint.endpoint_id,
      name: this.taskCreationForm.controls.name.value,
      tasks_fields: [],
      accesses: [],
    };

    // insert additional accesses
    for (let access of this.additionalAccesses) {
      let current = null;
      if (access.isUser) {
        current = {
          user_id: access.signee.user_id,
        };
      } else {
        current = {
          group_id: access.signee.group_id
        };
      }

      if (this.endpoint.sequential_status) {
        current.access_order = this.additionalAccesses.indexOf(access);
      }

      task.accesses.push(current);
    }

    // insert fields
    let files: File[] = [];
    let element: any;
    for (let i = 0; i < (<FormArray>this.taskCreationForm.controls.fields).length; i++) {
      element = (<FormArray>this.taskCreationForm.controls.fields).at(i);

      // ignore empty fields that are not required
      if (element.controls.value.value == null) {
        // safari always has value of file input as null
        if (element.controls.field_type.value === this.fieldTypes.FILE.toString()) {
          files = this.formFiles;
        } else {

        }
      } else if (element.controls.field_type.value === this.fieldTypes.FILE.toString()) {
        files = this.formFiles;
      } else if (element.controls.field_type.value === this.fieldTypes.DATE.toString()) {
        let datetime = new DatePipe(this.locale).transform(
          element.controls.value.value.toString(),
          'yyyy-MM-ddTHH:mm:ssZZZZZ', this.locale);
        task.tasks_fields.push({
          field_id: element.controls.field_id.value,
          value: datetime
        });
      } else {
        task.tasks_fields.push({
          field_id: element.controls.field_id.value,
          value: element.controls.value.value.toString()
        });
      }
    }

    this.taskData.createNewTask(task, files).subscribe((event: HttpEvent<any>) => {
      if (files.length > 0) {
        this.fileSubmitted = true;
      }
      if (event.type === HttpEventType.UploadProgress) {
        this.percentDone = Math.round(100 * event.loaded / event.total);
      } else if (event.type === HttpEventType.Response) {
        if (event.ok) {
          this.resetComponent();
          this.message = AlertMessage.TASK_CREATED;
        }
      }
    }, error => {
      this.message = AlertMessage.TASK_CREATION_FAILED;
      this.fileSubmitted = false;
    });

  }

  /**
   * Clear taskCreationForm and reset additionalAccesses and selected files.
   */
  resetComponent() {
    this.userData.getCurrentUser().then(user => {
      this.currentUser = user;
    });

    this.requiredFile = true;
    this.message = AlertMessage.NOT_SET;
    this.formFiles = [];
    this.fileSubmitted = false;
    this.percentDone = 0;
    this.additionalAccesses = [];

    this.taskCreationForm = new FormGroup({
      'name': new FormControl('', Validators.required),
      'fields': new FormArray([]),
      'accessSearchTerm': new FormControl(null),
    });
    this.fieldsAddedToForm = false;

    if (this.endpoint != null && !this.fieldsAddedToForm) {
      this.addFieldsToForm();
    }
  }

  /**
   * Check if the submit button should be disabled.
   */
  checkDisable(): boolean {
    if (this.endpoint != null) {
      return !this.taskCreationForm.valid || (this.additionalAccesses.length === 0 && this.endpoint.given_accesses.length === 0) || !this.requiredFile;
    } else {
      return !this.taskCreationForm.valid || !this.requiredFile;
    }
  }

  /**
   * Add selected files to formFiles.
   * @param files selected files
   */
  addFiles(files: FileList) {
    this.formFiles = [];
    for (let i = 0; i < files.length; i++) {
      this.formFiles.push(files[i]);
    }
    this.requiredFile = true;
  }

  /**
   * Check if page can be left without data loss.
   */
  canDeactivate() {
    if (this.taskCreationForm.dirty) {
      return (localStorage.getItem('localeId') === 'en' ?
        confirm(EN_Confirm.PROGRESS_LOST) :
        confirm(DE_Confirm.PROGRESS_LOST));
    }
    return true;
  }

  /**
   * Cancel creation and reset page.
   */
  cancelCreation() {
    this.fieldsAddedToForm = false;
    this.resetComponent();
  }
}
