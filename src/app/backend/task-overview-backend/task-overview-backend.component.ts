import {Component, OnInit} from '@angular/core';
import {TaskDataService} from '../../shared/task-data.service';
import {Task} from '../../models/task';
import {CacheMapService} from '../../shared/cache-map.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskTypes} from '../../models/task-types.enum';

/**
 * Task Overview of the backend.
 */
@Component({
  selector: 'app-task-overview-backend',
  templateUrl: './task-overview-backend.component.html',
  styleUrls: ['./task-overview-backend.component.css']
})
export class TaskOverviewBackendComponent implements OnInit {

  page = 1;
  entriesPerPage = 20;
  nextEnabled = true;
  tasks: Task[] = [];
  paramSubscription;
  taskType = TaskTypes.ACTIVE;

  constructor(private taskDataService: TaskDataService, private cache: CacheMapService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.paramSubscription = this.activatedRoute.queryParams.subscribe(params => {
      this.page = +params['page'] || 1;
      this.entriesPerPage = +params['entriesPerPage'] || 20;
    });
    this.loadTasks();
  }

  /**
   * Load next task page.
   */
  nextPage() {
    this.page++;
    this.loadTasks();
  }

  /**
   * Load previous task page.
   */
  previousPage() {
    this.page--;
    this.loadTasks();
    this.nextEnabled = true;
  }

  /**
   * Reload the task-overview.
   */
  reload() {
    this.cache.flush(this.taskType);
    this.loadTasks();
  }

  /**
   * Subroutine of reload.
   */
  loadTasks() {
    this.taskDataService.getTasks(this.page, this.entriesPerPage, this.taskType).subscribe(results => {
      this.nextEnabled = results.length === this.entriesPerPage;
      this.tasks = results;
    });
  }

  transformDate(date: string): string {
    return new Date(date).toLocaleString();
  }

}
