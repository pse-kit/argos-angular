import {Injectable} from '@angular/core';
import {TaskEvent} from '../models/task-event.enum';
import {HttpClient, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {Task} from '../models/task';
import {map} from 'rxjs/operators';
import {RouteGenerator} from './route-generator';

@Injectable({
  providedIn: 'root'
})
export class TaskDataService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Returns the details to a task with given id from webservice.
   */
  getTaskDetails(id: number) {
    return this.httpClient.get<Task>(RouteGenerator.getTasksIdRoute(id));
  }

  /**
   * Request to create a new task.
   */
  createNewTask(task: Task, files: File[]) {
    const formData = new FormData();
    formData.append('json', new Blob([JSON.stringify(task)], {
      type: 'multipart/form-data'
    }));

    for (let file of files) {
      formData.append('file', file, file.name);
    }

    let headers = new HttpHeaders();
    headers.set('Content-Type', 'multipart/form-data');

    const req = new HttpRequest('POST', RouteGenerator.getTasksRoute(), formData, {
      headers,
      reportProgress: true
    });
    return this.httpClient.request(req);
  }

  /**
   * Gets a list of tasks from the webservice.
   */
  getTasks(page: number, entriesPerPage: number, type: string) {
    const params = new HttpParams().set('type', type)
      .append('limit', String(entriesPerPage))
      .append('page', String(page));
    return this.httpClient.get<Task[]>(RouteGenerator.getTasksRoute(), {params}).pipe(map(x => {
      return x['tasks'];
    }));
  }

  /**
   * Sends a task-event to a given task_id to the webservice
   */
  sendEvent(id: number, event: TaskEvent) {
    return this.httpClient.put(RouteGenerator.getTasksStatusRoute(id), {'event': event.toString()});
  }

  /**
   * Request to add a comment to a task with specific id.
   */
  addComment(id: number, comment: string) {
    return this.httpClient.post<Comment>(RouteGenerator.getTasksCommentsRoute(id), {'text': comment});
  }

  /**
   * Request to delete a task with specific id.
   */
  deleteTask(id: number) {
    return this.httpClient.delete(RouteGenerator.getTasksIdRoute(id), {observe: 'response'}).pipe(map((resp) => {
      return resp.status === 200;
    }));
  }

  /**
   * Gets comments to a specific task.
   */
  getComments(id: number, limit: number, page: number) {
    const params = new HttpParams().set('limit', String(limit)).append('page', String(page));
    return this.httpClient.get<Comment[]>(RouteGenerator.getTasksCommentsRoute(id), {params: params}).pipe(
      map(x => {
        return x['comments'];
      })
    );
  }

}
