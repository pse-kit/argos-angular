import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FrontendComponent} from './frontend/frontend.component';
import {HeaderFrontendComponent} from './header-frontend/header-frontend.component';
import {SidebarFrontendComponent} from './sidebar-frontend/sidebar-frontend.component';
import {TasksOwnedComponent} from './tasks/tasks-owned/tasks-owned.component';
import {RouterModule} from '@angular/router';
import {LoginFrontendComponent} from './login-frontend/login-frontend.component';
import {PreferenceFrontendComponent} from './preference-frontend/preference-frontend.component';
import {MessageComponent} from './message/message.component';
import {TasksHistoryComponent} from './tasks/tasks-history/tasks-history.component';
import {TaskDetailComponent} from './tasks/task-detail/task-detail.component';
import {TaskCreationComponent} from './tasks/task-creation/task-creation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TasksSigningComponent} from './tasks/tasks-signing/tasks-signing.component';
import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {FrontendRoutingModule} from './frontend-routing.module';
import {TaskOverviewComponent} from './tasks/task-overview/task-overview.component';
import {NgbTooltipModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {AlertModule} from '../alert/alert.module';
import {FlatpickrModule} from 'angularx-flatpickr';


@NgModule({
  declarations: [
    FrontendComponent,
    HeaderFrontendComponent,
    SidebarFrontendComponent,
    TasksOwnedComponent,
    LoginFrontendComponent,
    PreferenceFrontendComponent,
    MessageComponent,
    TasksHistoryComponent,
    TaskDetailComponent,
    TaskCreationComponent,
    TasksSigningComponent,
    BreadcrumbComponent,
    TaskOverviewComponent
  ],
  imports: [
    CommonModule,
    AlertModule,
    NgbTypeaheadModule,
    NgbTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    FlatpickrModule.forRoot(),
    FrontendRoutingModule
  ],
  exports: [
    RouterModule
  ]
})
export class FrontendModule {
}
