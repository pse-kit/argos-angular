import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {EndpointDataService} from '../../../shared/endpoint-data.service';
import {UserDataService} from '../../../shared/user-data.service';
import {User} from '../../../models/user';
import {forkJoin, Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {SignatureTypes} from '../../../models/signature-types.enum';
import {FieldTypes} from '../../../models/field-types.enum';
import {Endpoint} from '../../../models/endpoint';
import {Group} from '../../../models/group';
import {CanComponentDeactivate} from '../../../shared/can-deactivate.guard';
import {AlertMessage} from '../../../alert/alert-messages';
import {DE_Confirm, EN_Confirm} from '../../../models/confirm.language';

/**
 * Component for creating an endpoint.
 */
@Component({
  selector: 'app-endpoint-creation',
  templateUrl: './endpoint-creation.component.html',
  styleUrls: ['./endpoint-creation.component.css']
})
export class EndpointCreationComponent implements OnInit, CanComponentDeactivate {

  searchInput;
  endpointCreationForm: FormGroup;
  accesses: any[] = [];
  signatureTypes = Object.values(SignatureTypes);
  fieldTypes = Object.values(FieldTypes);
  message: AlertMessage;

  constructor(private endpointData: EndpointDataService, private userData: UserDataService) {
  }

  /**
   * Get the form fields set in the endpointCreationForm.
   */
  get fields(): FormArray {
    return this.endpointCreationForm.get('fields') as FormArray;
  }

  ngOnInit() {
    this.endpointCreationForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'description': new FormControl(null),
      'signatureType': new FormControl(this.signatureTypes[0], Validators.required),
      'sequentialStatus': new FormControl(false, Validators.required),
      'deletableStatus': new FormControl(false, Validators.required),
      'fields': new FormArray([new FormGroup({
        'type': new FormControl(this.fieldTypes[0], Validators.required),
        'value': new FormControl(null, Validators.required),
        'required': new FormControl(false)
      })]),
      'emailNotificationStatus': new FormControl(false, Validators.required),
      'allowAdditionalAccessesStatus': new FormControl(false, Validators.required),
      'insertBefore': new FormControl(false, Validators.required),
      'accesses': new FormControl(null),
    });
  }

  /**
   * Search for user and groups matching the parameter text.
   * @param text search string.
   */
  search = (text: Observable<string>) =>
    text.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        forkJoin(this.userData.userSuggestions(term), this.userData.groupSuggestions(term))
      ), map(result => result[0].concat(result[1]).slice(0, 10)));

  /**
   * Format the search result to show only the name.
   */
  formatter = (result: any) => result.name;

  /**
   * Add a required signature.
   */
  addSignature(sig: any) {
    const isUser = sig.item.user_id !== undefined;
    this.accesses.push({signee: sig.item, isUser: isUser});
    setTimeout(() => this.searchInput = '', 1);
  }

  /**
   * Add an form field to the endpointCreationForm.
   */
  addFormField() {
    (<FormArray>this.endpointCreationForm.controls.fields).push(new FormGroup(
      {
        'type': new FormControl(this.fieldTypes[0], Validators.required),
        'value': new FormControl(null, Validators.required),
        'required': new FormControl(false)
      }));
  }

  /**
   * Reset the endpointCreationForm.
   */
  resetForm() {
    this.message = AlertMessage.NOT_SET;
    const control = <FormArray>this.endpointCreationForm.controls['fields'];
    for (let i = control.length - 1; i >= 0; i--) {
      control.removeAt(i);
    }
    this.endpointCreationForm.reset();
    control.push(new FormGroup({
      'type': new FormControl(this.fieldTypes[0], Validators.required),
      'value': new FormControl(null, Validators.required),
      'required': new FormControl(false)
    }));
    this.endpointCreationForm.get('signatureType').setValue(this.signatureTypes[0]);
    this.endpointCreationForm.get('sequentialStatus').setValue(false);
    this.endpointCreationForm.get('deletableStatus').setValue(false);
    this.endpointCreationForm.get('emailNotificationStatus').setValue(false);
    this.endpointCreationForm.get('allowAdditionalAccessesStatus').setValue(false);
    this.endpointCreationForm.get('insertBefore').setValue(false);
    this.endpointCreationForm.get('description').setValue(null);
    this.accesses = [];
  }

  /**
   * Remove an form field from the endpointCreationForm.
   */
  removeField(j: number) {
    (<FormArray>this.endpointCreationForm.controls.fields).removeAt(j);
  }

  /**
   * Remove a set signature.
   * @param index of the signature.
   */
  removeAccess(index: number) {
    this.accesses.splice(index, 1);
  }

  /**
   * Checks if the buttons should be disabled.
   */
  checkDisable() {
    return (!this.endpointCreationForm.valid ||
      !(this.endpointCreationForm.get('allowAdditionalAccessesStatus').value) && this.accesses.length === 0 ||
      (<FormArray>this.endpointCreationForm.controls.fields).length < 1);
  }

  /**
   * Submit the endpointCreationForm. Generates json for the webservice.
   */
  onSubmit() {
    this.message = AlertMessage.NOT_SET;

    let endpoint: Endpoint = {} as any;
    endpoint.name = this.endpointCreationForm.controls.name.value;
    endpoint.description = this.endpointCreationForm.controls.description.value;
    endpoint.allow_additional_accesses_status = this.endpointCreationForm.controls.allowAdditionalAccessesStatus.value;
    endpoint.deletable_status = this.endpointCreationForm.controls.deletableStatus.value;
    endpoint.email_notification_status = this.endpointCreationForm.controls.emailNotificationStatus.value;
    endpoint.signature_type = this.endpointCreationForm.controls.signatureType.value;
    endpoint.sequential_status = this.endpointCreationForm.controls.sequentialStatus.value;
    endpoint.additional_access_insert_before_status = this.endpointCreationForm.controls.insertBefore.value;

    // Set fields
    endpoint.fields = [];
    let formArray = (<FormArray>this.endpointCreationForm.controls.fields);
    for (let i = 0; i < formArray.length; i++) {
      const type = (<FormGroup>formArray.at(i)).controls.type.value.toString();
      const name = (<FormGroup>formArray.at(i)).controls.value.value.toString();
      endpoint.fields.push({'field_type': type, 'name': name, 'required_status': (<FormGroup>formArray.at(i)).controls.required.value});
    }

    // Set given_accesses
    endpoint.given_accesses = [];
    for (let i = 0; i < this.accesses.length; i++) {
      if (this.accesses[i].isUser) {
        endpoint.given_accesses.push({'access_order': i, 'user_id': (<User>this.accesses[i].signee).user_id});
      } else {
        endpoint.given_accesses.push({'access_order': i, 'group_id': (<Group>this.accesses[i].signee).group_id});
      }
    }

    this.endpointData.createEndpoint(endpoint).subscribe(
      data => {
        this.resetForm();
        this.message = AlertMessage.ENDPOINT_CREATED;
      },
      error => {
        this.message = AlertMessage.ENDPOINT_CREATION_FAILED;
      }
    );
  }

  canDeactivate() {
    if (this.endpointCreationForm.dirty) {
      return (localStorage.getItem('localeId') === 'en' ?
        confirm(EN_Confirm.PROGRESS_LOST) :
        confirm(DE_Confirm.PROGRESS_LOST));
    }
    return true;
  }
}

