import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {TaskDetailComponent} from './task-detail.component';
import {BehaviorSubject, of, throwError} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskDataService} from '../../../shared/task-data.service';
import {UserDataService} from '../../../shared/user-data.service';
import {FileDataService} from '../../file-data.service';
import {CacheMapService} from '../../../shared/cache-map.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModule} from '../../../alert/alert.module';
import {AlertMessage} from '../../../alert/alert-messages';
import {TaskEvent} from '../../../models/task-event.enum';

describe('TaskDetailComponent', () => {
  let component: TaskDetailComponent;
  let fixture: ComponentFixture<TaskDetailComponent>;
  const taskDataSpy = jasmine.createSpyObj('TaskDataService', ['getComments', 'getTaskDetails', 'addComment', 'sendEvent', 'deleteTask']);
  const userDataSpy = jasmine.createSpyObj('UserDataService', ['getCurrentUser']);
  const fileDataSpy = jasmine.createSpyObj('FileDataService', ['getFilesByUrl']);
  const cacheSpy = jasmine.createSpyObj('CacheMapService', ['flush']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(fakeAsync(() => {
    taskDataSpy.getComments.calls.reset();
    taskDataSpy.getTaskDetails.calls.reset();
    taskDataSpy.addComment.calls.reset();
    taskDataSpy.sendEvent.calls.reset();
    taskDataSpy.deleteTask.calls.reset();
    fileDataSpy.getFilesByUrl.calls.reset();
    cacheSpy.flush.calls.reset();
    routerSpy.navigateByUrl.calls.reset();
    userDataSpy.getCurrentUser.calls.reset();

    taskDataSpy.getComments.and.returnValue(of(comments));
    userDataSpy.getCurrentUser.and.returnValue(of(user).toPromise());
    taskDataSpy.sendEvent.and.returnValue(of());

    TestBed.configureTestingModule({
      declarations: [TaskDetailComponent],
      imports: [FormsModule, ReactiveFormsModule, AlertModule],
      providers: [{provide: ActivatedRoute, useClass: FakeRoute}, {provide: TaskDataService, useValue: taskDataSpy},
        {provide: UserDataService, useValue: userDataSpy}, {provide: FileDataService, useValue: fileDataSpy},
        {provide: CacheMapService, useValue: cacheSpy}, {provide: Router, useValue: routerSpy}]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(TaskDetailComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      tick();
    });
  }));

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should show the task correctly on display', fakeAsync(() => {
    expect(component.task).toBe(dummyTask);

    // Infos and Buttons
    const title = fixture.nativeElement.querySelector('#task_title').textContent;
    const mode = fixture.nativeElement.querySelector('#modus').textContent;
    const status = fixture.nativeElement.querySelector('#status').textContent;
    const creator = fixture.nativeElement.querySelector('#creator').textContent;
    const creationDate = fixture.nativeElement.querySelector('#creationDate').textContent;
    const lastEdit = fixture.nativeElement.querySelector('#lastEdit').textContent;
    const refresh_button = fixture.nativeElement.querySelector('#refresh_button');
    const reject_button = fixture.nativeElement.querySelector('#reject_button');
    expect(title).toBe('ID: 4 Spa\u00df');
    expect(mode).toBe('Parallel');
    expect(status).toBe('pending');
    expect(creator).toBe('Noah Wahl(noah.ares@yahoo.de)');
    // disabled because values depending on system the test is running
    // expect(creationDate).toBe('14.3.2019, 15:17:53');
    // expect(lastEdit).toBe('14.3.2019, 15:35:28');
    expect(refresh_button).not.toBeNull();
    expect(reject_button).not.toBeNull();

    // Signatur-Button
    const sign_button: HTMLButtonElement = fixture.nativeElement.querySelector('#button_after_download');
    const download_first_warning = fixture.nativeElement.querySelector('#download_first_warning');
    expect(sign_button).not.toBeNull();
    expect(sign_button.disabled).toBeTruthy();
    expect(download_first_warning).not.toBeNull();

    // Signatures, Comments and Files missing
  }));

  it('should reload the page clicking the refresh Button', fakeAsync(() => {
    const refresh_button: HTMLButtonElement = fixture.nativeElement.querySelector('#refresh_button');
    taskDataSpy.getTaskDetails.and.returnValue(of(dummyTask));
    refresh_button.click();
    fixture.detectChanges();
    tick();
    expect(cacheSpy.flush).toHaveBeenCalled();
    expect(taskDataSpy.getTaskDetails).toHaveBeenCalled();
    expect(taskDataSpy.getComments).toHaveBeenCalled();
  }));

  it('should show an error message if task could not loaded', fakeAsync(() => {
    const refresh_button: HTMLButtonElement = fixture.nativeElement.querySelector('#refresh_button');
    taskDataSpy.getTaskDetails.and.returnValue(throwError(new Error()));
    refresh_button.click();
    fixture.detectChanges();
    tick();
    expect(component.message).toBe(AlertMessage.CAN_NOT_RELOAD);
  }));

  it('should return the correct hash', () => {
    expect(component.getUserHash(user)).toBe('95182e107bdc51bb31d7bc22bd9994ef');
  });


  it('should send a reject event on rejecting a task', fakeAsync(() => {
    component.reject(() => true);
    fixture.detectChanges();
    tick();
    expect(taskDataSpy.sendEvent).toHaveBeenCalledWith(4, TaskEvent.DENIED);
  }));

  it('should show a error, when task was not successfull rejected', fakeAsync(() => {
    taskDataSpy.sendEvent.and.returnValue(throwError(new Error()));
    component.reject(() => true);
    fixture.detectChanges();
    tick();
    expect(taskDataSpy.sendEvent).toHaveBeenCalledWith(4, TaskEvent.DENIED);
  }));

  it('should withdraw a task', fakeAsync(() => {
    taskDataSpy.deleteTask.and.returnValue(of(true));
    component.deleteTask(() => true);
    tick();
    fixture.detectChanges();
    expect(taskDataSpy.deleteTask).toHaveBeenCalledWith(4);
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/tasks/owned');
  }));

  it('should withdraw a task fail', fakeAsync(() => {
    taskDataSpy.deleteTask.and.returnValue(throwError(new Error()));
    component.deleteTask(() => true);
    tick();
    fixture.detectChanges();
    expect(taskDataSpy.deleteTask).toHaveBeenCalledWith(4);
    expect(component.message).toBe(AlertMessage.SOMETHING_WENT_WRONG);
  }));

  it('should add an comment', fakeAsync(() => {
    taskDataSpy.getTaskDetails.and.returnValue(of(dummyTask));
    taskDataSpy.getComments.and.returnValue(of(comments));
    taskDataSpy.addComment.and.returnValue(of(true));
    component.comment = 'Hallo Welt';
    component.addComment();
    tick();
    fixture.detectChanges();
    expect(taskDataSpy.addComment).toHaveBeenCalledWith(4, 'Hallo Welt');
    expect(component.comment).toBe('');
  }));

  it('should sign a task', fakeAsync(() => {
    taskDataSpy.sendEvent.and.returnValue(of(true));
    taskDataSpy.getTaskDetails.and.returnValue(of(dummyTask));
    taskDataSpy.getComments.and.returnValue(of(comments));
    component.sign();
    tick();
    fixture.detectChanges();
    expect(component.isSigned).toBeTruthy();
  }));

  it('should show an error if signing not successfull', fakeAsync(() => {
    taskDataSpy.sendEvent.and.returnValue(throwError(new Error()));
    taskDataSpy.getTaskDetails.and.returnValue(of(dummyTask));
    taskDataSpy.getComments.and.returnValue(of(comments));
    component.sign();
    tick();
    fixture.detectChanges();
    expect(component.message).toBe(AlertMessage.SOMETHING_WENT_WRONG);
  }));
});

const dummyTask = {
  'accesses': [{
    'access_order': 0,
    'access_status': 'signed',
    'event_date': '2019-03-14T14:35:28+00:00',
    'group': null,
    'user': {'email_address': 'moritzleitner@gmail.com', 'name': 'Moritz Leitner', 'user_id': 1}
  }, {
    'access_order': 0,
    'access_status': 'signed',
    'event_date': '2019-03-14T14:30:14+00:00',
    'group': null,
    'user': {'email_address': 'noah.ares@yahoo.de', 'name': 'Noah Wahl', 'user_id': 5}
  }, {
    'access_order': 0,
    'access_status': 'viewed',
    'event_date': '2019-03-14T14:18:28+00:00',
    'group': null,
    'user': {'email_address': 'N-Schuler@gmx.de', 'name': 'Nicolas Schuler', 'user_id': 3}
  }, {
    'access_order': 0,
    'access_status': 'signed',
    'event_date': '2019-03-14T14:22:46+00:00',
    'group': null,
    'user': {'email_address': 'wendy.yi@web.de', 'name': 'Wendy Yi', 'user_id': 4}
  }, {
    'access_order': 0,
    'access_status': 'viewed',
    'event_date': '2019-03-14T14:19:08+00:00',
    'group': null,
    'user': {'email_address': 'julius.haecker@web.de', 'name': 'Julius H\u00e4cker', 'user_id': 2}
  }],
  'can_sign': 'afterDownload',
  'creation_date': '2019-03-14T14:17:53+00:00',
  'creator': {'email_address': 'noah.ares@yahoo.de', 'name': 'Noah Wahl', 'user_id': 5},
  'endpoint_id': 2,
  'name': 'Spa\u00df',
  'sequential_status': false,
  'signature_type': 'buttonAfterDownload',
  'task_id': 4,
  'task_status': 'pending',
  'tasks_fields': [{'field': {'field_type': 'file', 'name': 'NPC Problem'}, 'value': 'klausur2ws1718_ohne_loesung.pdf'}]
};

const comments = [{
  'comment_date': '2019-03-14T14:22:34+00:00',
  'comment_id': 1,
  'creator': {'email_address': 'wendy.yi@web.de', 'name': 'Wendy Yi', 'user_id': 4},
  'task_id': 4,
  'text': 'TGI!!! :D'
}];

const user = {'email_address': 'N-Schuler@gmx.de', 'name': 'Nicolas Schuler', 'user_id': 3};
const task = {task: dummyTask};

class FakeRoute {
  data = new BehaviorSubject(task);
}
