import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {CacheMapService} from './cache-map.service';


@Injectable()
export class CachingInterceptor implements HttpInterceptor {
  constructor(private cache: CacheMapService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRequestCachable(req)) {
      return next.handle(req);
    }
    const cachedResponse = this.cache.get(req);
    if (cachedResponse !== null) {
      return of(cachedResponse);
    }
    return next.handle(req).pipe(
      tap(event => {
        if (event instanceof HttpResponse && event.status === 200) {
          this.cache.put(req, event);
        }
      })
    );
  }

  private isRequestCachable(req: HttpRequest<any>) {
    if (req.method !== 'GET') {
      if (req.url.indexOf('tasks') > -1) {
        this.cache.flush('tasks');
      } else if (req.url.indexOf('endpoints') > -1) {
        this.cache.flush('endpoints');
      } else if (req.url.indexOf('preferences') > -1) {
        this.cache.flush('preferences');
      }
      return false;
    }
    return (req.url.indexOf('tasks') > -1 || req.url.indexOf('endpoints') > -1 || req.url.indexOf('preferences') > -1);
  }
}
