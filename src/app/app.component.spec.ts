import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {Title} from '@angular/platform-browser';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {EN_Breadcrumb} from './models/breadcrumb.language';
import {Router, Routes} from '@angular/router';
import {Component} from '@angular/core';


describe('AppComponent', () => {
  const titleSpy = jasmine.createSpyObj('Title', ['setTitle']);
  let router: Router;
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        DummyComponent
      ],
      imports: [RouterTestingModule.withRoutes(routes), LoadingBarHttpClientModule],
      providers: [{provide: Title, useValue: titleSpy}]
    }).compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call createTitle after an navigation', fakeAsync(() => {
    fixture.ngZone.run(() => {
      const spy = spyOn(component, 'createTitle');
      router.navigate(['login']);
      tick();
      expect(spy).toHaveBeenCalled();
    });
  }));

  it('should set the title correctly based on the route', fakeAsync(() => {
    fixture.ngZone.run(() => {
      // Test 1
      router.navigate(['tasks/owned']);
      tick();
      expect(titleSpy.setTitle).toHaveBeenCalledWith('Owned Tasks');

      // Test 2
      router.navigate(['login']);
      tick();
      expect(titleSpy.setTitle).toHaveBeenCalledWith('Login');

      // Test 3
      router.navigate(['preferences']);
      tick();
      expect(titleSpy.setTitle).toHaveBeenCalledWith('Preferences');

      // Test 4
      router.navigate(['tasks/details/1']);
      tick();
      expect(titleSpy.setTitle).toHaveBeenCalledWith('Task Details');
    });
  }));


});

@Component({
  selector: 'app-dummy',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.css']
})
class DummyComponent {
}


// get current locale
const lang = EN_Breadcrumb;

const routes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    component: DummyComponent,
    data: {breadcrumb: 'Login'}
  },
  {
    path: '', data: {
      breadcrumb: lang.HOME
    }, component: DummyComponent, children: [
      {path: '', pathMatch: 'full', redirectTo: 'tasks/owned'},
      {
        path: 'tasks', data: {breadcrumb: lang.TASKS},
        children: [
          {path: '', pathMatch: 'full', redirectTo: 'owned'},
          {
            path: 'details/:id', data: {
              breadcrumb: lang.TASK_DETAILS
            }, component: DummyComponent
          },
          {
            path: 'owned', data: {
              breadcrumb: lang.TASKS_OWNED
            }, component: DummyComponent
          },
          {
            path: 'to-sign', data: {
              'breadcrumb': lang.TASKS_TOSIGN
            }, component: DummyComponent
          },
          {
            path: 'create/:id', data: {
              'breadcrumb': lang.TASK_CREATE
            }, component: DummyComponent
          },
          {
            path: 'history', data: {
              'breadcrumb': lang.TASK_HISTORY
            }, component: DummyComponent
          }
        ]
      },
      {
        path: 'preferences', data: {
          'breadcrumb': lang.PREFERENCES
        }, component: DummyComponent
      },
    ]
  }];
