import {Component, OnInit} from '@angular/core';

/**
 * Container for the whole frontend view.
 */
@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
  styleUrls: ['./frontend.component.css']
})
export class FrontendComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
