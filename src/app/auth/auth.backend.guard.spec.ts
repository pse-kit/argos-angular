import {AuthBackendGuard} from './auth.backend.guard';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {AuthorizationService} from './authorization.service';

describe('AuthBackendGuard', () => {
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let authServiceSpy;
  let authBackendGuard: AuthBackendGuard;

  // not used by AuthBackendGuard
  const next: ActivatedRouteSnapshot = null;

  beforeEach(() => {
    routerSpy.navigateByUrl.calls.reset();
    authServiceSpy = jasmine.createSpyObj('AuthorizationService', ['setAdmin', 'isAuthenticated', 'isAdmin']);
    authBackendGuard = new AuthBackendGuard(routerSpy as Router, authServiceSpy as AuthorizationService);
    localStorage.clear();
  });

  it('Should set admin to true in the AuthorizationService', () => {
    const state = {url: '/admin/login'};
    authBackendGuard.canActivate(next, state as RouterStateSnapshot);

    expect(authServiceSpy.setAdmin).toHaveBeenCalledWith(true);
  });

  it('should navigate an already logged in admin away from the login page', () => {
    const state = {url: '/admin/login'};
    authServiceSpy.isAdmin.and.returnValue(true);
    authServiceSpy.isAuthenticated.and.returnValue('true');

    expect(authBackendGuard.canActivate(next, state as RouterStateSnapshot)).toBeFalsy();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/admin/endpoints/management');
  });

  it('should let a not authenticated admin stay on the login page', () => {
    const state = {url: '/admin/login'};
    authServiceSpy.isAdmin.and.returnValue(true);
    authServiceSpy.isAuthenticated.and.returnValue(false);

    expect(authBackendGuard.canActivate(next, state as RouterStateSnapshot)).toBeTruthy();
  });

  it('should allow an authenticated admin to navigate where he want to', () => {
    const state = {url: '/admin/endpoints/create'};
    authServiceSpy.isAdmin.and.returnValue(true);
    authServiceSpy.isAuthenticated.and.returnValue(true);

    expect(authBackendGuard.canActivate(next, state as RouterStateSnapshot)).toBeTruthy();
  });

  it('should not allow an unauthorized admin to navigate where he want to, setup a redirectUrl and navigate to the login page', () => {
    const state = {url: '/admin/endpoints/create'};
    authServiceSpy.isAdmin.and.returnValue(true);
    authServiceSpy.isAuthenticated.and.returnValue(false);
    expect(authBackendGuard.canActivate(next, state as RouterStateSnapshot)).toBeFalsy();
    expect(localStorage.getItem('redirectUrl')).toBe('/admin/endpoints/create');
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/admin/login');
  });


});
