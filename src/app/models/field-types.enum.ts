export enum FieldTypes {
  DATE = 'datetime',
  FILE = 'file',
  TEXT = 'textfield',
  CHECKBOX = 'checkbox'
}
