import {TestBed} from '@angular/core/testing';

import {CacheMapService} from './cache-map.service';
import {HttpRequest, HttpResponse} from '@angular/common/http';

describe('CacheMapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));
  const req: HttpRequest<any> = new HttpRequest('GET', '/test/route');
  const resp: HttpResponse<any> = new HttpResponse();


  it('should be created', () => {
    const service: CacheMapService = TestBed.get(CacheMapService);
    expect(service).toBeTruthy();
  });

  it('should put entry', () => {
    const service: CacheMapService = TestBed.get(CacheMapService);
    service.put(req, resp);
    expect(service.cacheMap.has('/test/route'));
  });

  it('should get entry', () => {
    const service: CacheMapService = TestBed.get(CacheMapService);
    service.cacheMap.set(req.urlWithParams, {url: req.urlWithParams, response: resp});
    expect(service.get(req)).toBe(resp);
  });

  it('should not get entry', () => {
    const service: CacheMapService = TestBed.get(CacheMapService);
    expect(service.get(req)).toBeNull();
  });

  it('should flush only /test/route/to_flush', () => {
    const service: CacheMapService = TestBed.get(CacheMapService);
    const flush_req: HttpRequest<any> = new HttpRequest('GET', '/test/route/to_flush');
    service.put(flush_req, resp);
    service.put(req, resp);
    service.flush('to_flush');
    expect(service.cacheMap.has('/test/route/to_flush')).toBe(false);
    expect(service.cacheMap.has('/test/route')).toBe(true);
  });
});
