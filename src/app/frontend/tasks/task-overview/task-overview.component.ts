import {Component, OnInit} from '@angular/core';
import {Task} from '../../../models/task';
import {TaskDataService} from '../../../shared/task-data.service';
import {CacheMapService} from '../../../shared/cache-map.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-task-overview',
  templateUrl: './task-overview.component.html',
  styleUrls: ['./task-overview.component.css']
})
export class TaskOverviewComponent implements OnInit {


  tasks: Task[] = [];
  page = 1;
  entriesPerPage = 20;
  nextEnabled = false;
  paramSubscription;
  navigationSubscription;
  taskType: string;

  constructor(protected taskDataService: TaskDataService, protected cache: CacheMapService, protected router: Router, protected activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.paramSubscription = this.activatedRoute.queryParams.subscribe(params => {
      this.page = +params['page'] || 1;
      this.entriesPerPage = +params['entriesPerPage'] || 20;
    });
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.loadTasksFromResolver();
      }
    });
    this.loadTasksFromResolver();
  }

  nextPage() {
    this.page++;
    this.router.navigate(['./'], {queryParams: {page: this.page, entriesPerPage: this.entriesPerPage}, relativeTo: this.activatedRoute});
  }

  previousPage() {
    this.page--;
    this.router.navigate(['./'], {queryParams: {page: this.page, entriesPerPage: this.entriesPerPage}, relativeTo: this.activatedRoute});
    this.nextEnabled = true;
  }

  reload() {
    this.cache.flush(this.taskType);
    this.router.navigate(['./'], {queryParams: {page: this.page, entriesPerPage: this.entriesPerPage}, relativeTo: this.activatedRoute});
  }

  loadTasksFromResolver() {
    this.activatedRoute.data.subscribe((data: { tasks: Task[] }) => {
      this.nextEnabled = data.tasks.length >= this.entriesPerPage;
      this.tasks = data.tasks;
    });
  }
}
