export enum DE_Breadcrumb {
  HOME = 'Startseite',
  TASKS = 'Aufträge',
  TASK_DETAILS = 'Auftragdetails',
  TASKS_OWNED = 'Eigene Aufträge',
  TASKS_TOSIGN = 'Zu Signierende Aufträge',
  TASK_CREATE = 'Auftrag Erstellen',
  TASK_HISTORY = 'Historie',
  PREFERENCES = 'Einstellungen',
  ENDPOINTS_MANAGEMENT = 'Endpunkte Verwalten',
  ENDPOINT_CREATION = 'Endpunkt Erstellen',
  ENDPOINT_DETAILS = 'Endpunktdetails',
  TASK_OVERVIEW = 'Auftragsübersicht',
  ERROR = '404'
}

export enum EN_Breadcrumb {
  HOME = 'Home',
  TASKS = 'Tasks',
  TASK_DETAILS = 'Task Details',
  TASKS_OWNED = 'Owned Tasks',
  TASKS_TOSIGN = 'Tasks To Sign',
  TASK_CREATE = 'Create Task',
  TASK_HISTORY = 'History',
  PREFERENCES = 'Preferences',
  ENDPOINTS_MANAGEMENT = 'Endpoint Management',
  ENDPOINT_CREATION = 'Create Endpoint',
  ENDPOINT_DETAILS = 'Endpoint Details',
  TASK_OVERVIEW = 'Task Overview',
  ERROR = '404'
}
