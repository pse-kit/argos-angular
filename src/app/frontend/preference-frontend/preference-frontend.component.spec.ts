import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {PreferenceFrontendComponent} from './preference-frontend.component';
import {UserDataService} from '../../shared/user-data.service';
import {of} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('PreferenceFrontendComponent', () => {
  let component: PreferenceFrontendComponent;
  let fixture: ComponentFixture<PreferenceFrontendComponent>;
  const userDataSpy = jasmine.createSpyObj('UserDataService', ['setLanguagePreference', 'setNotificationPreference']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreferenceFrontendComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [{provide: UserDataService, useValue: userDataSpy}, {
        provide: ActivatedRoute,
        useValue:
          {data: of({preferences: {language: 'en', email_notification: false}})}
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    localStorage.clear();
    fixture = TestBed.createComponent(PreferenceFrontendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.preferences.language).toBe('en');
    expect(component.preferences.email_notification).toBe(false);
    expect(localStorage.getItem('localeId')).toBe('en');
  }));

  it('should change the notification status when changing the slider', fakeAsync(() => {
    userDataSpy.setNotificationPreference.and.returnValue(of(true));
    tick();
    component.preferences.email_notification = true;
    const slider: HTMLInputElement = fixture.nativeElement.querySelector('#inputEmailNotfication');
    slider.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(userDataSpy.setNotificationPreference).toHaveBeenCalledWith(true);
    expect(slider.value).toBe('on');
  }));

  it('should change the language settings, when selecting another', fakeAsync(() => {
    userDataSpy.setLanguagePreference.and.returnValue(of(true));
    tick();
    component.preferences.language = 'de';
    const spy = jasmine.createSpyObj('Location', ['replace']);
    component.changeLanguage(spy as Location);
    expect(userDataSpy.setLanguagePreference).toHaveBeenCalledWith('de');
    expect(spy.replace).toHaveBeenCalledWith('/de/preferences');
    expect(localStorage.getItem('localeId')).toBe('de');
  }));
});
