import {AuthorizationService} from './authorization.service';
import {fakeAsync, tick} from '@angular/core/testing';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {get as getCookie, remove as removeCookie, set as setCookie} from 'es-cookie';
import {of, throwError} from 'rxjs';
import {CacheMapService} from '../shared/cache-map.service';

describe('AuthorizationService', () => {
  const httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);
  const cache = new CacheMapService();
  let authService: AuthorizationService;

  beforeEach(() => {
    // spy reset
    httpSpy.get.calls.reset();
    httpSpy.post.calls.reset();
    httpSpy.delete.calls.reset();
    authService = new AuthorizationService(httpSpy as HttpClient, cache as CacheMapService);
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should set all variables correct after successful login response | user', () => fakeAsync(() => {
    login(false, true);
    // Check state
    expect(authService.isAuthenticated()).toBe(true, 'user not authenticated');
    expect(getCookie('user-access-token')).toBe('user-access-token', 'accessToken not set as a cookie');
    expect(getCookie('user-refresh-token')).toBe('user-refresh-token', 'refreshToken not set as a cookie');
    expect(authService.isAdmin()).toBe(false, 'User is recognized as an admin');
    expect(authService.getAccessToken()).toBe('user-access-token', 'access-token not accessible');
    expect(authService.getRefreshToken()).toBe('user-refresh-token', 'refresh-token not accessible');
    expect(authService.getCookieNamePrefix()).toBe('user', 'name prefix for cookie wrong');
  }));

  it('should set all variables correct after successful login response | admin', fakeAsync(() => {
    login(true, true);
    // Check state
    expect(authService.isAuthenticated()).toBe(true, 'admin not authenticated');
    expect(getCookie('admin-access-token')).toBe('admin-access-token', 'accessToken not set as a cookie');
    expect(getCookie('admin-refresh-token')).toBe('admin-refresh-token', 'refreshToken not set as a cookie');
    expect(authService.isAdmin()).toBe(true, 'Admin is recognized as an user');
    expect(authService.getAccessToken()).toBe('admin-access-token', 'access-token not accessible');
    expect(authService.getRefreshToken()).toBe('admin-refresh-token', 'refresh-token not accessible');
    expect(authService.getCookieNamePrefix()).toBe('admin', 'name prefix for cookie wrong');
  }));

  it('should recognize correct refresh cookie and set authenticated to true | admin', () => {
    authService.setAdmin(true);
    setCookie('admin-refresh-token', 'refresh-token');
    setCookie('admin-access-token', 'access-token');
    expect(authService.getAccessToken()).toBe('access-token');
    expect(authService.isAuthenticated()).toBe(true);
    expect(authService.isAdmin()).toBe(true);
  });

  it('should not recognize wrong refresh cookie and not set authenticated to true | admin', () => {
    authService.setAdmin(true);
    // remove old cookie
    removeCookie('admin-refresh-token');
    setCookie('user-refresh-token', 'refresh-token');

    expect(authService.isAuthenticated()).toBe(false);
  });

  it('should recognize correct refresh cookie and set authenticated to true | user', () => {
    authService.setAdmin(false);
    setCookie('user-refresh-token', 'refresh-token');
    expect(authService.isAuthenticated()).toBe(true);
    expect(authService.isAdmin()).toBe(false);
  });

  it('should not recognize wrong refresh cookie and not set authenticated to true | user', () => {
    authService.setAdmin(false);
    // remove old cookie
    removeCookie('user-refresh-token');
    setCookie('admin-refresh-token', 'refresh-token');

    expect(authService.isAuthenticated()).toBe(false);
  });

  it('should reset all variables after logout | user', fakeAsync(() => {
    login(false, true);
    // logout
    httpSpy.delete.and.returnValue(of(new HttpResponse({status: 200})));
    authService.logout();
    tick();
    // Simulate ClearService
    authService.clear();

    expect(authService.isAuthenticated()).toBe(false, 'user is authenticated');
    expect(getCookie('user-access-token')).toBe(undefined, 'accessToken set as a cookie');
    expect(getCookie('user-refresh-token')).toBe(undefined, 'refreshToken set as a cookie');
    expect(authService.isAdmin()).toBe(false, 'admin set');
    expect(authService.getAccessToken()).toBe(undefined, 'access-token accessible');
    expect(authService.getRefreshToken()).toBe(undefined, 'refresh-token accessible');
    expect(authService.getCookieNamePrefix()).toBe('user', 'name prefix for cookie wrong');
  }));

  it('should reset all variables after logout | admin', fakeAsync(() => {
    login(true, true, false);
    // logout
    httpSpy.delete.and.returnValue(of(new HttpResponse({status: 200})));
    authService.logout();
    tick();
    // Simulate ClearService
    authService.clear();

    expect(authService.isAuthenticated()).toBe(false, 'admin is authenticated');
    expect(getCookie('admin-access-token')).toBe(undefined, 'accessToken set as a cookie');
    expect(getCookie('admin-refresh-token')).toBe(undefined, 'refreshToken set as a cookie');
    expect(authService.isAdmin()).toBe(false, 'admin set');
    expect(authService.getAccessToken()).toBe(undefined, 'access-token accessible');
    expect(authService.getRefreshToken()).toBe(undefined, 'refresh-token accessible');
    expect(authService.getCookieNamePrefix()).toBe('user', 'name prefix for cookie wrong');
  }));

  it('should refresh the access-token, when response was successful', fakeAsync(() => {
    login(false, true);

    httpSpy.post.and.returnValue(of(new HttpResponse({status: 201, body: {'access_token': 'new-access-token'}})));
    authService.tokenRefresh().subscribe(next => expect(next).toBe('new-access-token'));
    tick();
    expect(authService.getAccessToken()).toBe('new-access-token');
    expect(getCookie('user-access-token')).toBe('new-access-token');

  }));

  it('should return null if token refresh was not successful', fakeAsync(() => {
    login(false, true);
    httpSpy.post.and.returnValue(throwError(new HttpErrorResponse({status: 401})));
    authService.tokenRefresh().subscribe(next => {
    }, err => {
      expect(err).toBe('token refresh error');
    });
    tick();
    expect(authService.getAccessToken()).toBe('user-access-token');
    expect(getCookie('user-access-token')).toBe('user-access-token');
  }));

  it('should return an error if login failed', fakeAsync(() => {
    authService.setAdmin(false);
    // login
    httpSpy.post.and.returnValue(throwError(new HttpErrorResponse({
      status: 401
    })));
    authService.login('Nicolas', '1234', false, false).subscribe(next => {
    }, err => expect(err.status).toBe(401));
    tick();
  }));

  function login(admin: boolean, successful: boolean, rememberMe = true) {
    authService.setAdmin(admin);
    if (!successful) {
      httpSpy.post.and.returnValue(throwError(new HttpErrorResponse({status: 401})));
    } else {
      // login
      httpSpy.post.and.returnValue(of(new HttpResponse({
        status: 201,
        body: {
          'refresh_token': authService.getCookieNamePrefix() + '-refresh-token',
          'access_token': authService.getCookieNamePrefix() + '-access-token'
        },
      })));

    }
    authService.login('Nicolas', '1234', admin, rememberMe).subscribe(next => expect(next).toBe(successful));
    tick();
  }
});


