import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Task} from '../../../models/task';
import {EMPTY, Observable, of} from 'rxjs';
import {TaskDataService} from '../../../shared/task-data.service';
import {catchError, mergeMap, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskDetailResolverService implements Resolve<Task> {

  constructor(private taskDataService: TaskDataService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task> | Promise<Task> | Task {
    const id = +route.paramMap.get('id');

    return this.taskDataService.getTaskDetails(id).pipe(
      take(1),
      mergeMap(task => {
        return of(task);
      }), catchError(err => {
        if (state.url.includes('/details/')) {
          this.router.navigateByUrl('/tasks/owned');
        } else {
          this.router.navigateByUrl(state.url);
        }
        return EMPTY;
      })
    );
  }
}
