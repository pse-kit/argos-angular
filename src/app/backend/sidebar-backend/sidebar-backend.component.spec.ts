import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SidebarBackendComponent} from './sidebar-backend.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SidebarBackendComponent', () => {
  let component: SidebarBackendComponent;
  let fixture: ComponentFixture<SidebarBackendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarBackendComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarBackendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
