import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value.length > args) {
      const nextWhiteSpace = value.indexOf(' ', args);
      return nextWhiteSpace === -1 ? value : value.substr(0,) + ' [...]';
    }
    return value;
  }

}
