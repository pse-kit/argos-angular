import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {BreadcrumbComponent} from './breadcrumb.component';
import {Component} from '@angular/core';
import {EN_Breadcrumb} from '../../models/breadcrumb.language';
import {Router, RouterModule, Routes} from '@angular/router';

describe('BreadcrumbComponent', () => {
  let component: BreadcrumbComponent;
  let fixture: ComponentFixture<BreadcrumbComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BreadcrumbComponent,
        DummyComponent
      ],
      imports: [RouterModule.forRoot(routes)]
    }).compileComponents().then(() => {
      router = TestBed.get(Router);
      fixture = TestBed.createComponent(BreadcrumbComponent);
      component = fixture.componentInstance;
    });
  }));

  it('should create', fakeAsync(() => {
    fixture.ngZone.run(() => {
      router.navigateByUrl('/');
      tick();
      fixture.detectChanges();
      expect(component).toBeTruthy();
    });
  }));

  it('should rebuild breadcrumbs on router navigation correctly (Owned Tasks)', fakeAsync(() => {
    let call = false;
    component.breadcrumbs.subscribe(breadcrumbs => {
      if (router.url === '/tasks/owned') {
        expect(breadcrumbs[0].label).toBe('Home');
        expect(breadcrumbs[1].label).toBe('Tasks');
        expect(breadcrumbs[2].label).toBe('Owned Tasks');
      }
      call = true;
    });
    fixture.ngZone.run(() => {
      router.navigateByUrl('/tasks/owned');
      tick();
      expect(router.url).toBe('/tasks/owned');
    });
    tick();
    fixture.detectChanges();
    expect(call).toBeTruthy();
  }));


  it('should fire an event when a navigation is initiated', fakeAsync(() => {
    fixture.ngZone.run(() => {
      let call = false;
      component.breadcrumbs.subscribe(breadcrumbs => {
        if (router.url === '/tasks/to-sign') {
          call = true;
        }
      });
      router.navigateByUrl('/tasks/to-sign');
      tick();
      fixture.detectChanges();
      expect(call).toBeTruthy();
    });
  }));


  it('should rebuild breadcrumbs on router navigation correctly (Create Task)', fakeAsync(() => {
    let call = false;
    component.breadcrumbs.subscribe(breadcrumbs => {
      if (router.url === '/tasks/create/2') {
        expect(breadcrumbs[0].label).toBe('Home');
        expect(breadcrumbs[1].label).toBe('Tasks');
        expect(breadcrumbs[2].label).toBe('Create Task');
      }
      call = true;
    });
    fixture.ngZone.run(() => {
      router.navigateByUrl('/tasks/create/2');
      tick();
      expect(router.url).toBe('/tasks/create/2');
    });
    tick();
    fixture.detectChanges();
    expect(call).toBeTruthy();
  }));

  it('should rebuild breadcrumbs on router navigation correctly (Preferences)', fakeAsync(() => {
    let call = false;
    component.breadcrumbs.subscribe(breadcrumbs => {
      if (router.url === '/preferences') {
        expect(breadcrumbs[0].label).toBe('Home');
        expect(breadcrumbs[1].label).toBe('Preferences');
      }
      call = true;
    });
    fixture.ngZone.run(() => {
      router.navigateByUrl('/preferences');
      tick();
      expect(router.url).toBe('/preferences');
    });
    tick();
    fixture.detectChanges();
    expect(call).toBeTruthy();
  }));
});


@Component({
  selector: 'app-dummy',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./breadcrumb.component.css']
})
class DummyComponent {
}


// get current locale
const lang = EN_Breadcrumb;

const routes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    component: DummyComponent,
    data: {breadcrumb: 'Login'}
  },
  {
    path: '', data: {
      breadcrumb: lang.HOME
    }, component: DummyComponent, children: [
      {path: '', pathMatch: 'full', redirectTo: 'tasks/owned'},
      {
        path: 'tasks', data: {breadcrumb: lang.TASKS},
        children: [
          {path: '', pathMatch: 'full', redirectTo: 'owned'},
          {
            path: 'details/:id', data: {
              breadcrumb: lang.TASK_DETAILS
            }, component: DummyComponent
          },
          {
            path: 'owned', data: {
              breadcrumb: lang.TASKS_OWNED
            }, component: DummyComponent
          },
          {
            path: 'to-sign', data: {
              'breadcrumb': lang.TASKS_TOSIGN
            }, component: DummyComponent
          },
          {
            path: 'create/:id', data: {
              'breadcrumb': lang.TASK_CREATE
            }, component: DummyComponent
          },
          {
            path: 'history', data: {
              'breadcrumb': lang.TASK_HISTORY
            }, component: DummyComponent
          }
        ]
      },
      {
        path: 'preferences', data: {
          'breadcrumb': lang.PREFERENCES
        }, component: DummyComponent
      },
    ]
  }];

