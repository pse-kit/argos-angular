import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderFrontendComponent} from './header-frontend.component';
import {AuthorizationService} from '../../auth/authorization.service';
import {Router} from '@angular/router';
import {ClearService} from '../../shared/clear.service';
import {MessageDataService} from '../message-data.service';
import {UserDataService} from '../../shared/user-data.service';
import {of} from 'rxjs';

describe('HeaderFrontendComponent', () => {
  let component: HeaderFrontendComponent;
  let fixture: ComponentFixture<HeaderFrontendComponent>;
  let fakeAuth: FakeAuthorizationService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderFrontendComponent],
      providers: [{provide: AuthorizationService, useClass: FakeAuthorizationService},
        {provide: ClearService, useClass: FakeClearService}, {provide: Router, useValue: routerSpy},
        {provide: MessageDataService, useClass: FakeMessageService}, {provide: UserDataService, useClass: FakeUserService}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(HeaderFrontendComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      fakeAuth = TestBed.get(AuthorizationService);
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should logout correctly and redirect to user login', () => {
    component.logout();
    const spy = routerSpy.navigateByUrl as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];
    expect(navArgs).toBe('/login');
  });
});

class FakeAuthorizationService {

  logout() {
    return true;
  }
}

class FakeClearService {

  clearAllServices() {
    return true;
  }
}

class FakeMessageService {

  getMessagesToReadCount() {
    return of(42);
  }
}

class FakeUserService {

  async getCurrentUser() {
    return of({user_id: 1, name: 'Max Mustermann'});
  }
}
