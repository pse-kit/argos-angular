import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginBackendComponent} from './login-backend/login-backend.component';
import {AuthBackendGuard} from '../auth/auth.backend.guard';
import {BackendComponent} from './backend/backend.component';
import {EndpointManagementComponent} from './endpoints/endpoint-management/endpoint-management.component';
import {EndpointCreationComponent} from './endpoints/endpoint-creation/endpoint-creation.component';
import {EndpointDetailComponent} from './endpoints/endpoint-detail/endpoint-detail.component';
import {TaskOverviewBackendComponent} from './task-overview-backend/task-overview-backend.component';
import {CanDeactivateGuard} from '../shared/can-deactivate.guard';
import {ErrorPageComponent} from '../errors/error-page/error-page.component';
import {DE_Breadcrumb, EN_Breadcrumb} from '../models/breadcrumb.language';

const lang = localStorage.getItem('localeId') === 'en' ? EN_Breadcrumb : DE_Breadcrumb;
const routes: Routes = [
  {
    path: 'admin/login',
    pathMatch: 'full',
    component: LoginBackendComponent,
    canActivate: [AuthBackendGuard],
    data: {breadcrumb: 'Admin Login'}
  },
  {
    path: 'admin', component: BackendComponent, children: [
      {path: '', pathMatch: 'full', redirectTo: 'endpoints/management'},
      {path: 'endpoints', pathMatch: 'full', redirectTo: 'endpoints/management'},
      {
        path: 'endpoints/management', component: EndpointManagementComponent, data: {
          breadcrumb: lang.ENDPOINTS_MANAGEMENT
        }
      },
      {
        path: 'endpoints/creation',
        component: EndpointCreationComponent,
        data: {breadcrumb: lang.ENDPOINT_CREATION},
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'endpoints/details/:id', component: EndpointDetailComponent, data: {
          breadcrumb: lang.ENDPOINT_DETAILS
        }
      },
      {
        path: 'tasks', component: TaskOverviewBackendComponent, data: {
          breadcrumb: lang.TASK_OVERVIEW
        }
      }
    ], canActivate: [AuthBackendGuard]
  }, {
    path: '**', component: ErrorPageComponent, data: {
      breadcrumb: lang.ERROR
    }
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackendRoutingModule {
}
