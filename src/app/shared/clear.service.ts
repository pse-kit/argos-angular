import {Injectable, Injector} from '@angular/core';
import {AuthorizationService} from '../auth/authorization.service';
import {UserDataService} from './user-data.service';
import {MessageDataService} from '../frontend/message-data.service';

@Injectable({
  providedIn: 'root'
})
export class ClearService {
  services = [AuthorizationService, UserDataService, MessageDataService];

  constructor(private injector: Injector) {
  }

  /**
   * Clears all services.
   */
  clearAllServices() {
    for (const service of this.services) {
      (<Clearable>this.injector.get(service)).clear();
    }
  }
}

export interface Clearable {
  clear();
}
