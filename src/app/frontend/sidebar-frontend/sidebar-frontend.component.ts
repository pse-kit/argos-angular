import {Component, OnInit} from '@angular/core';
import {EndpointDataService} from '../../shared/endpoint-data.service';
import {Endpoint} from '../../models/endpoint';

@Component({
  selector: 'app-sidebar-frontend',
  templateUrl: './sidebar-frontend.component.html',
  styleUrls: ['./sidebar-frontend.component.css']
})
export class SidebarFrontendComponent implements OnInit {

  chevronTasks = false;
  chevronEndpoints = false;
  endpoints: Endpoint[] = [];
  page = 1;
  entriesPerPage = 10;
  allEndpointsLoaded = false;
  noEndpointsAvailable = false;
  standardTaskQueries = {page: 1, entriesPerPage: 20};

  constructor(private endpointService: EndpointDataService) {
  }

  ngOnInit() {
    this.loadEndpoints();
  }

  nextPage() {
    this.page++;
    this.loadEndpoints();
  }

  loadEndpoints() {
    this.endpointService.getEndpoints(this.page, this.entriesPerPage).subscribe((next) => {
      this.allEndpointsLoaded = next.length < this.entriesPerPage;
      if (this.endpoints.length === 0) {
        this.endpoints = next;
      } else {
        this.endpoints = this.endpoints.concat(next);
      }
      this.noEndpointsAvailable = this.endpoints.length === 0;
    });
  }

}
