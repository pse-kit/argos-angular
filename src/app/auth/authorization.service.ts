import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';
import {get as getCookie, remove as removeCookie, set as setCookie} from 'es-cookie';
import {CacheMapService} from '../shared/cache-map.service';
import {Clearable} from '../shared/clear.service';
import {RouteGenerator} from '../shared/route-generator';
import {environment} from '../../environments/environment';


/**
 * AuthorizationService handles the authorization with the webservice.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements Clearable {

  private authenticated = false;
  private admin = false;
  private rememberMe: boolean;
  private refreshToken = '';
  private accessToken = '';

  constructor(private httpClient: HttpClient,
              private cache: CacheMapService) {
  }

  private static addDomain(attribute) {
    if (environment.production) {
      attribute.domain = RouteGenerator.getDomain();
    }
    return attribute;
  }

  /**
   *  Returns true if the current logged in user is recognized as an admin.
   */
  isAdmin(): boolean {
    return this.admin;
  }

  /**
   * Returns the authentication status of a user.
   */
  isAuthenticated(): boolean {
    // try to find a refresh-token
    return this.authenticated || this.getRefreshToken() !== undefined;
  }

  /**
   * Returns the currently set access-token.
   * @return access-token or undefined if access-token not set.
   */
  getAccessToken(): string | undefined {
    const token = getCookie(this.getCookieNamePrefix() + Token.ACCESS);
    if (token !== undefined && this.accessToken === '') {
      // User leaved the site
      this.accessToken = token;
    }
    return token;
  }

  /**
   * Returns the currently set refresh-token
   * @return refresh-token or undefined if refresh-token not set.
   */
  getRefreshToken(): string | undefined {
    const token = getCookie(this.getCookieNamePrefix() + Token.REFRESH);
    if (token !== undefined) {
      this.authenticated = true;
      if (this.refreshToken === '') {
        // User leaved the site
        this.refreshToken = token;
      }
    }
    return token;
  }

  /**
   * Sends a request to the webservice for a new access-token.
   @return the refresh-token provided as an observable. If the request fails the observable will throw an error.
   */
  tokenRefresh(): Observable<string> {
    const cookieNamePrefix = this.getCookieNamePrefix();

    return this.httpClient.post(RouteGenerator.getRefreshRoute(), {}, {observe: 'response'})
      .pipe(map(response => {
        const body: any = response.body;
        this.accessToken = body['access_token'];
        setCookie(cookieNamePrefix + Token.ACCESS, this.accessToken,
          AuthorizationService.addDomain({expires: new Date(new Date().getTime() + 15 * 60000)}));
        return this.accessToken;
      }), catchError(err => {
        return throwError('token refresh error');
      }));
  }

  /**
   * Login to webservice.
   * Cookie setup if login was successful.
   *
   * @param username username of the user.
   * @param password password of the user.
   * @param adminLogin determines whether a an admin or an user logged in.
   * @param rememberMe determines whether the cookies should be session managed.
   */
  login(username: string, password: string, adminLogin: boolean, rememberMe: boolean): Observable<boolean> {
    this.rememberMe = rememberMe;
    let loginPath = new HttpParams();
    loginPath = loginPath.set('role', adminLogin ? Role.ADMIN : Role.USER);
    const cookieNamePrefix = adminLogin ? Role.ADMIN : Role.USER;

    return this.httpClient.post(RouteGenerator.getLoginRoute(), {
      'username': username,
      'password': password
    }, {params: loginPath, observe: 'response'}).pipe(map(response => {
      const body: any = response.body;
      // set up variables correctly
      this.authenticated = true;
      this.admin = adminLogin;
      this.refreshToken = body['refresh_token'];
      this.accessToken = body['access_token'];

      // set up cookies
      setCookie(cookieNamePrefix + Token.ACCESS, body['access_token'],
        AuthorizationService.addDomain({expires: new Date(new Date().getTime() + 15 * 60000)}));
      if (this.rememberMe) {
        // 1 day for refresh cookie to expire
        setCookie(cookieNamePrefix + Token.REFRESH, body['refresh_token'],
          AuthorizationService.addDomain({expires: new Date(new Date().getTime() + (24 * 60 * 60 * 1000))}));
      } else {
        // no expire-date provided means, the cookie is deleted when the Browser is closed.
        setCookie(cookieNamePrefix + Token.REFRESH, body['refresh_token'],
          AuthorizationService.addDomain({}));
      }
      return true;
    }));

  }

  /**
   * Invalidates the refresh- and access-token by sending a request to the webservice.
   * Deletes set cookies for authorization.
   */
  logout(): void {
    // make access-token invalid
    this.httpClient.delete(RouteGenerator.getLogoutAccessRoute(),
      {headers: new HttpHeaders({'Authorization': 'Bearer ' + this.accessToken})})
      .pipe(retry(2)).subscribe();

    // make refresh-token invalid
    this.httpClient.delete(RouteGenerator.getLogoutRefreshRoute(),
      {headers: new HttpHeaders({'Authorization': 'Bearer ' + this.refreshToken})})
      .pipe(retry(2)).subscribe();

    const cookieNamePrefix = this.getCookieNamePrefix();

    // remove all set cookies
    removeCookie(cookieNamePrefix + Token.ACCESS, AuthorizationService.addDomain({}));
    removeCookie(cookieNamePrefix + Token.REFRESH, AuthorizationService.addDomain({}));
  }

  /*
   * Returns the prefix for the currently needed cookie.
   */
  getCookieNamePrefix(): string {
    return this.admin === true ? Role.ADMIN : Role.USER;
  }


  /**
   * Decides whether the user should be treated as an admin or as an user.
   */
  setAdmin(value: boolean): void {
    this.admin = value;
  }

  clear(): void {
    this.authenticated = false;
    this.admin = false;
    this.refreshToken = '';
    this.accessToken = '';
    this.cache.flush('');
  }
}

/**
 * Unified cookie suffix.
 */
enum Token {
  ACCESS = '-access-token',
  REFRESH = '-refresh-token'
}

/**
 * Unified name for admin and user.
 */
enum Role {
  ADMIN = 'admin',
  USER = 'user'
}
