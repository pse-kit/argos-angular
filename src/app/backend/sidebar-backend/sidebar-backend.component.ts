import {Component, OnInit} from '@angular/core';

/**
 * Sidebar for the backend.
 */
@Component({
  selector: 'app-sidebar-backend',
  templateUrl: './sidebar-backend.component.html',
  styleUrls: ['./sidebar-backend.component.css']
})
export class SidebarBackendComponent implements OnInit {

  chevronEndpoints = false;
  standardQueries = {page: 1, entriesPerPage: 20};

  constructor() {
  }

  ngOnInit() {
  }

}
