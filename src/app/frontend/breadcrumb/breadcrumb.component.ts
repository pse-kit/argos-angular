import {Component} from '@angular/core';
import {ActivatedRoute, ChildActivationEnd, Router} from '@angular/router';
import {BreadCrumb} from './breadcrumb';
import {filter, map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css'],
})
export class BreadcrumbComponent {
  routerEvent = this.router.events.pipe(filter(event => event instanceof ChildActivationEnd),
    map(event => this.buildBreadCrumb(this.activatedRoute.root)));
  breadcrumbs = this.routerEvent.pipe(startWith(this.buildBreadCrumb(this.activatedRoute.root)));

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router) {
  }


  buildBreadCrumb(route: ActivatedRoute): Array<BreadCrumb> {
    let breadcrumbs: BreadCrumb[] = [];
    let currentRoute = route.snapshot;

    while (currentRoute !== null) {
      if (currentRoute.data['breadcrumb'] !== undefined && currentRoute.routeConfig.path !== null) {
        let path;
        if (breadcrumbs.length === 0) {
          path = '/' + currentRoute.routeConfig.path;
        } else {
          path = breadcrumbs[breadcrumbs.length - 1].url + '/' + currentRoute.routeConfig.path;
        }
        breadcrumbs = [...breadcrumbs, {label: currentRoute.routeConfig.data['breadcrumb'], url: path}];
      }
      currentRoute = currentRoute.firstChild;
    }
    return breadcrumbs;
  }
}
