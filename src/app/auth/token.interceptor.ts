import {BehaviorSubject, EMPTY, Observable} from 'rxjs';

import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import {AuthorizationService} from './authorization.service';
import {RouteGenerator} from '../shared/route-generator';
import {ClearService} from '../shared/clear.service';
import {Router} from '@angular/router';

/**
 * Injects the Authorization-Header into requests. Provides functionality to catch
 * an unauthorized access, signaled by the HTTP status code 422. It will try to refresh the token,
 * if the refresh fails, the user will get logged out.
 */
@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private authService: AuthorizationService, private clearService: ClearService, private router: Router) {
  }

  /**
   * Adds the Authorization-Header with the set access-token to the request.
   */
  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({setHeaders: {Authorization: 'Bearer ' + token}});
  }

  /**
   * Intercepts the sending of a request to the webservice.
   * Catches 422 error (Unauthorized).
   * @param req
   * @param next
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

    // To show requests
    if (req.urlWithParams.startsWith(RouteGenerator.getLoginRoute())) {
      // We are login in, so no need to intercept
      return next.handle(req);
    }

    // Token refresh
    if (req.url.startsWith(RouteGenerator.getRefreshRoute())) {
      return next.handle(req.clone({setHeaders: {Authorization: 'Bearer ' + this.authService.getRefreshToken()}}));
    }

    // Logout
    if (req.url.startsWith(RouteGenerator.getLogoutRefreshRoute()) || req.url.startsWith(RouteGenerator.getLogoutAccessRoute())) {
      return next.handle(req);
    }


    return next.handle(this.addToken(req, this.authService.getAccessToken())).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {
          // Got an Token invalid
          if ((<HttpErrorResponse>error).status === 422) {
            return this.handle422Error(req, next);
          } else if ((<HttpErrorResponse>error).status === 0) {
            // Connection lost, logout
            return this.logoutUser();
          } else {
            return next.handle(req);
          }
        }
      }));
  }

  /**
   * Handles a 422 error (Unauthorized), tries then to refresh the token. All other request will wait for this call to finish.
   * If an error occurred while refreshing the token, the user will be forced to logout.
   */
  handle422Error(req: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      // wait until the token comes back from the refresh call.
      this.tokenSubject.next(null);

      return this.authService.tokenRefresh().pipe(
        switchMap((newToken: string) => {
          if (newToken) {
            this.tokenSubject.next(newToken);
            return next.handle(this.addToken(req, newToken));
          }

          // no token provided, means we should logout now.
          return this.logoutUser();
        }),
        catchError((error) => {
          // If an exception occurs while trying to refresh the token. The user will be forced to logout.
          return this.logoutUser();
        }),
        finalize(() => {
          this.isRefreshingToken = false;
        }));
    } else {
      return this.tokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(token => {
          return next.handle(this.addToken(req, token));
        }));
    }
  }

  /**
   * Clears all Services and logs the user out.
   */
  logoutUser() {
    const admin = this.authService.isAdmin();
    const loginRoute = admin ? '/admin/login' : '/login';
    this.authService.logout();
    this.clearService.clearAllServices();
    this.authService.setAdmin(admin);
    this.router.navigateByUrl(loginRoute);
    return EMPTY;
  }
}
