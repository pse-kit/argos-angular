import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Task} from '../../../models/task';
import {EMPTY, Observable, of} from 'rxjs';
import {TaskDataService} from '../../../shared/task-data.service';
import {catchError, mergeMap, take} from 'rxjs/operators';
import {TaskTypes} from '../../../models/task-types.enum';

@Injectable({
  providedIn: 'root'
})
export class TaskOverviewResolverService implements Resolve<Task[]> {

  constructor(private taskDataService: TaskDataService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task[]> | Promise<Task[]> | Task[] {
    let type: string;
    if (state.url.includes('/owned')) {
      type = TaskTypes.OWNED;
    } else if (state.url.includes('/to-sign')) {
      type = TaskTypes.TOSIGN;
    } else {
      type = TaskTypes.ARCHIVED;
    }
    const page = +route.queryParamMap.get('page') || 1;
    const entriesPerPage = +route.queryParamMap.get('entriesPerPage') || 20;
    return this.taskDataService.getTasks(page, entriesPerPage, type).pipe(
      take(1),
      mergeMap(tasks => {
        return of(tasks);
      }), catchError(err => {
        if (err.status === 0) {
          this.router.navigateByUrl('/login');
        }
        if (state.url.includes('/tasks')) {
          this.router.navigateByUrl('/tasks/owned');
        } else {
          this.router.navigateByUrl(state.url);
        }
        return EMPTY;
      })
    );
  }

}
