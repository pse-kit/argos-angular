import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {LoginFrontendComponent} from './login-frontend.component';
import {AuthorizationService} from '../../auth/authorization.service';
import {asyncScheduler, Observable, of, throwError} from 'rxjs';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertMessage} from '../../alert/alert-messages';
import {AlertModule} from '../../alert/alert.module';


describe('LoginFrontendComponent', () => {
  let component: LoginFrontendComponent;
  let fixture: ComponentFixture<LoginFrontendComponent>;
  let fakeAuth: FakeAuthorizationService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const routeSpy = {snapshot: jasmine.createSpyObj('ActivatedRoute', ['queryParams'])};
  let username: HTMLInputElement;
  let password: HTMLInputElement;
  let checkbox: HTMLSelectElement;

  beforeEach(async(() => {
    routerSpy.navigateByUrl.calls.reset();
    routeSpy.snapshot.queryParams.calls.reset();
    TestBed.configureTestingModule({
      declarations: [LoginFrontendComponent],
      imports: [FormsModule, ReactiveFormsModule, AlertModule],
      providers: [{provide: AuthorizationService, useClass: FakeAuthorizationService},
        {provide: Router, useValue: routerSpy}, {provide: ActivatedRoute, useValue: routeSpy}]
    })
      .compileComponents().then(() => {
      localStorage.clear();
      fixture = TestBed.createComponent(LoginFrontendComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      // Access to FakeAuthService
      fakeAuth = TestBed.get(AuthorizationService);

      // Setup DOM Elements for access
      username = fixture.nativeElement.querySelector('#inputUsername');
      password = fixture.nativeElement.querySelector('#inputPassword');
      checkbox = fixture.nativeElement.querySelector('#rememberMe');
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy('Component could not be created');
  });

  it('should show a fail message when providing wrong credentials', () => {
    // Set wrong credentials
    username.value = 'SomethingWrong';
    password.value = 'SomethingEvenWronger';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();
    // Login failed
    expect(component.message).toBe(AlertMessage.LOGIN_FAILED);

    // Check Message shows up
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#loginFailed')).not.toBeNull('Login failed message not shown');
  });

  it('should show a connectionError when simulating one', () => {
    // Set up a ConnectionError
    fakeAuth.connectionError = true;
    // Set wrong credentials
    username.value = 'SomethingWrong';
    password.value = 'SomethingEvenWronger';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();

    // Login failed
    expect(component.message).toBe(AlertMessage.CONNECTION_ERROR);

    // Check Message shows up
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#connectionError')).not.toBeNull('Connection error message not shown');
  });

  it('should navigate to /tasks/owned when providing correct credentials', () => {
    // Set correct credentials
    username.value = 'testUser';
    password.value = 'testPassword';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();

    // args passed to router.navigateByUrl() spy
    const spy = routerSpy.navigateByUrl as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];

    // expecting navigation to /tasks/owned
    expect(navArgs).toBe('/tasks/owned',
      'should navigate to tasks owned');
  });

  it('should disable the submit button, when login-request not completed', fakeAsync(() => {
    const submit: HTMLButtonElement = fixture.nativeElement.querySelector('#submitButton');
    // Set correct credentials
    username.value = 'testAsyncUser';
    password.value = 'testAsyncPassword';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();
    expect(submit.disabled).toBe(false);

    // Submit
    component.onSubmit();
    fixture.detectChanges();

    // Button should now be disabled
    expect(submit.disabled).toBe(true);

    // next value of observable
    tick();
    fixture.detectChanges();

    // After request button should be activated again
    expect(submit.disabled).toBe(false);
  }));

  it('should show ldap failed if, ldap failed', () => {
    // Set up a ConnectionError
    fakeAuth.connectionError = false;
    fakeAuth.ldapFailed = true;
    // Set wrong credentials
    username.value = 'SomethingWrong';
    password.value = 'SomethingEvenWronger';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();

    // Login failed
    expect(component.message).toBe(AlertMessage.LDAP_FAILED);

    // Check Message shows up
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#ldapFailed')).not.toBeNull('LdapFailed message not shown');
  });


  it('should do nothing when clicked on the submit button, and the form is invalid', () => {
    const submit: HTMLButtonElement = fixture.nativeElement.querySelector('#submitButton');
    // Set correct credentials
    username.value = 'testAsyncUser';
    username.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    // Submit
    component.onSubmit();
    fixture.detectChanges();

    expect(component.submitted).toBe(true);
    expect(component.loading).toBe(false);
    expect(routerSpy.navigateByUrl).not.toHaveBeenCalled();
    expect(spyOn(fakeAuth, 'login')).not.toHaveBeenCalled();
  });

});


class FakeAuthorizationService {
  connectionError: Boolean = false;
  ldapFailed: Boolean = false;

  login(username: string, password: string, adminLogin: boolean, rememberMe: boolean) {
    if (this.connectionError === true) {
      return throwError(new HttpErrorResponse({status: 501}));
    } else {
      if (username === 'testUser' && password === 'testPassword' && adminLogin === false) {
        return new Observable(observer => observer.next(true));
      } else if (username === 'testAsyncUser' && password === 'testAsyncPassword' && adminLogin === false) {
        return of(true, asyncScheduler);
      } else {
        if (this.ldapFailed) {
          return throwError(new HttpErrorResponse({status: 500}));
        } else {
          return throwError(new HttpErrorResponse({status: 401}));
        }
      }
    }

  }
}
