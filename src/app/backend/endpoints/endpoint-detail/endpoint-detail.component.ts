import {Component, Input, OnInit} from '@angular/core';
import {Endpoint} from '../../../models/endpoint';


@Component({
  selector: 'app-endpoint-detail',
  templateUrl: './endpoint-detail.component.html',
  styleUrls: ['./endpoint-detail.component.css']
})
export class EndpointDetailComponent implements OnInit {

  @Input() endpoint: Endpoint;

  constructor() {
  }

  ngOnInit() {
  }

}
