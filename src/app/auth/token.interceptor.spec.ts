import {TokenInterceptorService} from './token.interceptor';
import {AuthorizationService} from './authorization.service';
import {ClearService} from '../shared/clear.service';
import {Router} from '@angular/router';
import {HttpErrorResponse, HttpParams, HttpRequest, HttpResponse} from '@angular/common/http';
import {RouteGenerator} from '../shared/route-generator';
import {of, throwError} from 'rxjs';
import {fakeAsync, tick} from '@angular/core/testing';


describe('TokenInterceptor', () => {

  let authSpy;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const nextRequestHandler = jasmine.createSpyObj('HttpHandler', ['handle']);
  const clearSpy = jasmine.createSpyObj('ClearService', ['clearAllServices']);

  let tokenInterceptor: TokenInterceptorService;

  beforeEach(() => {
    // reset Spy
    authSpy = jasmine.createSpyObj('AuthorizationService', ['isAdmin', 'logout', 'setAdmin',
      'tokenRefresh', 'getAccessToken', 'getRefreshToken']);
    routerSpy.navigateByUrl.calls.reset();
    nextRequestHandler.handle.calls.reset();
    clearSpy.clearAllServices.calls.reset();

    tokenInterceptor = new TokenInterceptorService(authSpy as AuthorizationService, clearSpy as ClearService,
      routerSpy as Router);
  });

  it('should not modify a login request', () => {
    // admin
    let req = new HttpRequest('POST', RouteGenerator.getLoginRoute(), {something: 'something'}, {params: new HttpParams().set('role', 'admin')});

    tokenInterceptor.intercept(req, nextRequestHandler);
    expect(nextRequestHandler.handle).toHaveBeenCalledWith(req);

    // user
    req = new HttpRequest('POST', RouteGenerator.getLoginRoute(), {something: 'something'}, {params: new HttpParams().set('role', 'user')});
    tokenInterceptor.intercept(req, nextRequestHandler);
    expect(nextRequestHandler.handle).toHaveBeenCalledWith(req);
  });

  it('should put the refresh token in the Authorization-Header, when the refresh route is called', () => {
    const req = new HttpRequest('GET', RouteGenerator.getRefreshRoute());
    authSpy.getRefreshToken.and.returnValue('refresh-token');
    tokenInterceptor.intercept(req, nextRequestHandler);
    expect(nextRequestHandler.handle.calls.mostRecent().args[0].headers.get('Authorization')).toBe('Bearer refresh-token');
  });

  it('should not modify a logout request', () => {
    const reqAccess = new HttpRequest('DELETE', RouteGenerator.getLogoutAccessRoute());
    tokenInterceptor.intercept(reqAccess, nextRequestHandler);
    expect(nextRequestHandler.handle.calls.mostRecent().args[0]).toBe(reqAccess);

    const reqRefresh = new HttpRequest('DELETE', RouteGenerator.getLogoutAccessRoute());
    tokenInterceptor.intercept(reqRefresh, nextRequestHandler);
    expect(nextRequestHandler.handle.calls.mostRecent().args[0]).toBe(reqRefresh);
  });

  it('should normally put the access-token in the Authorization-Header', fakeAsync(() => {
    const req = new HttpRequest('GET', RouteGenerator.getEndpointsRoute());
    authSpy.getAccessToken.and.returnValue('access-token');
    nextRequestHandler.handle.and.returnValue(of(new HttpResponse({status: 200})));
    tokenInterceptor.intercept(req, nextRequestHandler);
    expect(nextRequestHandler.handle.calls.mostRecent().args[0].headers.get('Authorization')).toBe('Bearer access-token');
  }));

  it('should logout the user on status code 0 and return to the login page', fakeAsync(() => {
    const req = new HttpRequest('GET', RouteGenerator.getEndpointsRoute());
    authSpy.getAccessToken.and.returnValue('access-token');
    authSpy.isAdmin.and.returnValue(false);
    nextRequestHandler.handle.and.returnValue(throwError(new HttpErrorResponse({status: 0})));
    tick();
    tokenInterceptor.intercept(req, nextRequestHandler).subscribe();
    tick();
    expect(authSpy.logout).toHaveBeenCalled();
    expect(clearSpy.clearAllServices).toHaveBeenCalled();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/login');
  }));

  it('should refresh the access-token when its invalid | success', fakeAsync(() => {
    const req = new HttpRequest('GET', RouteGenerator.getEndpointsRoute());
    authSpy.getAccessToken.and.returnValue('access-token');
    authSpy.isAdmin.and.returnValue(false);
    nextRequestHandler.handle.and.returnValue(throwError(new HttpErrorResponse({status: 422})));
    authSpy.tokenRefresh.and.returnValue(of('new-access-token'));
    tick();
    tokenInterceptor.intercept(req, nextRequestHandler).subscribe();
    tick();
    expect(authSpy.tokenRefresh).toHaveBeenCalled();
    expect(nextRequestHandler.handle.calls.mostRecent().args[0].headers.get('Authorization')).toBe('Bearer new-access-token');
  }));

  it('should refresh the access-token when its invalid | failure  -> logout', fakeAsync(() => {
    const req = new HttpRequest('GET', RouteGenerator.getEndpointsRoute());
    authSpy.getAccessToken.and.returnValue('access-token');
    authSpy.isAdmin.and.returnValue(false);
    nextRequestHandler.handle.and.returnValue(throwError(new HttpErrorResponse({status: 422})));
    authSpy.tokenRefresh.and.returnValue(throwError('token-refresh-error'));
    tick();
    tokenInterceptor.intercept(req, nextRequestHandler).subscribe();
    tick();
    expect(authSpy.tokenRefresh).toHaveBeenCalled();
    expect(authSpy.logout).toHaveBeenCalled();
    expect(clearSpy.clearAllServices).toHaveBeenCalled();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/login');
  }));

  it('any other error should get throw back to the caller ', fakeAsync(() => {
    const req = new HttpRequest('GET', RouteGenerator.getEndpointsRoute());
    authSpy.getAccessToken.and.returnValue('access-token');
    authSpy.isAdmin.and.returnValue(false);
    nextRequestHandler.handle.and.returnValue(throwError(new HttpErrorResponse({status: 1})));
    tick();
    tokenInterceptor.intercept(req, nextRequestHandler).subscribe(
      x => fail('not an Error'),
      error => expect(error.status).toBe(1)
    );
    tick();
  }));

});
