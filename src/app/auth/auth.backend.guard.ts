import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthorizationService} from './authorization.service';

/**
 * Authorization Guard for the backend module.
 * The Guard protects routes from unauthorized access.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthBackendGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthorizationService) {
  }

  /**
   * If the user ist not authorized, he will be redirected to /admin/login, else he wil be redirected to
   * his desired url or /admin/endpoints/management if no redirectUrl is provided.
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.auth.setAdmin(true);

    // Login backend
    if (state.url.startsWith('/admin/login')) {
      if (this.auth.isAuthenticated() && this.auth.isAdmin()) {
        this.router.navigateByUrl('/admin/endpoints/management');
        return false;
      } else {
        return true;
      }
    }

    // Access control for backend
    if (!this.auth.isAuthenticated() && state.url.startsWith('/admin/')) {
      if (!state.url.startsWith('/admin/endpoints/management')) {
        localStorage.setItem('redirectUrl', state.url);
      }
      this.router.navigateByUrl('/admin/login');
      return false;
    }
    return true;
  }
}
