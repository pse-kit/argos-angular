import {RouterModule, Routes} from '@angular/router';
import {LoginFrontendComponent} from './login-frontend/login-frontend.component';
import {AuthFrontendGuard} from '../auth/auth.frontend.guard';
import {FrontendComponent} from './frontend/frontend.component';
import {TasksOwnedComponent} from './tasks/tasks-owned/tasks-owned.component';
import {TasksSigningComponent} from './tasks/tasks-signing/tasks-signing.component';
import {TaskCreationComponent} from './tasks/task-creation/task-creation.component';
import {TasksHistoryComponent} from './tasks/tasks-history/tasks-history.component';
import {PreferenceFrontendComponent} from './preference-frontend/preference-frontend.component';
import {NgModule} from '@angular/core';
import {TaskDetailComponent} from './tasks/task-detail/task-detail.component';
import {TaskDetailResolverService} from './tasks/task-detail/task-detail-resolver.service';
import {TaskOverviewResolverService} from './tasks/task-overview/task-overview-resolver.service';
import {PreferencesResolverService} from './preference-frontend/preferences-resolver.service';
import {CanDeactivateGuard} from '../shared/can-deactivate.guard';
import {DE_Breadcrumb, EN_Breadcrumb} from '../models/breadcrumb.language';
import {environment} from '../../environments/environment';


const addRoutes = environment.production ? [] : [{path: 'de', redirectTo: '/tasks/owned'}, {path: 'en', redirectTo: '/tasks/owned'}];

// get current locale
const lang = localStorage.getItem('localeId') === 'en' ? EN_Breadcrumb : DE_Breadcrumb;

const routes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    component: LoginFrontendComponent,
    canActivate: [AuthFrontendGuard],
    data: {breadcrumb: 'Login'}
  }, ...addRoutes,
  {
    path: '', data: {
      breadcrumb: lang.HOME
    }, component: FrontendComponent, children: [
      {path: '', pathMatch: 'full', redirectTo: 'tasks/owned'},
      {
        path: 'tasks', data: {breadcrumb: lang.TASKS},
        children: [
          {path: '', pathMatch: 'full', redirectTo: 'owned'},
          {
            path: 'details/:id', data: {
              breadcrumb: lang.TASK_DETAILS
            }, component: TaskDetailComponent, resolve: {
              task: TaskDetailResolverService
            }
          },
          {
            path: 'owned', data: {
              breadcrumb: lang.TASKS_OWNED
            }, component: TasksOwnedComponent, resolve: {
              tasks: TaskOverviewResolverService
            }, runGuardsAndResolvers: 'always'
          },
          {
            path: 'to-sign', data: {
              'breadcrumb': lang.TASKS_TOSIGN
            }, component: TasksSigningComponent, resolve: {
              tasks: TaskOverviewResolverService
            }, runGuardsAndResolvers: 'always'
          },
          {
            path: 'create/:id', data: {
              'breadcrumb': lang.TASK_CREATE
            }, component: TaskCreationComponent, canDeactivate: [CanDeactivateGuard]
          },
          {
            path: 'history', data: {
              'breadcrumb': lang.TASK_HISTORY
            }, component: TasksHistoryComponent, resolve: {
              tasks: TaskOverviewResolverService
            }, runGuardsAndResolvers: 'always'
          }
        ]
      },
      {
        path: 'preferences', data: {
          'breadcrumb': lang.PREFERENCES
        }, component: PreferenceFrontendComponent, resolve: {
          preferences: PreferencesResolverService
        }
      },
    ], canActivate: [AuthFrontendGuard]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontendRoutingModule {
}
