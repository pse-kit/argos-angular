import {Component, OnInit} from '@angular/core';
import {Message} from '../../models/message';
import {MessageDataService} from '../message-data.service';


/**
 * Message component for frontend.
 */
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  page = 1;
  entriesPerPage = 20;
  allMessagesLoaded = false;
  messages: Message[] = [];
  selected = false;
  selectedMessage: Message;

  constructor(private messageDataService: MessageDataService) {
  }

  ngOnInit() {
    this.loadMessages();
  }

  /**
   * Load next message page.
   */
  nextPage() {
    this.page++;
    this.loadMessages();
  }

  /**
   * Reload messages.
   */
  reload() {
    this.page = 1;
    this.selected = false;
    this.messages = [];
    this.loadMessages();
  }

  /**
   * Subroutine of reload and nextPage.
   */
  loadMessages() {
    this.messageDataService.getMessages(this.page, this.entriesPerPage).subscribe((next) => {
      this.allMessagesLoaded = next.length < this.entriesPerPage;
      if (this.messages.length === 0) {
        this.messages = next;
      } else {
        this.messages = this.messages.concat(next);
      }
      this.updateReadCount();
    });
  }

  /*
  * Called when message with index is selected.
   */
  selectMessage(index: number) {
    this.selected = true;
    this.selectedMessage = this.messages[index];
    if (!this.selectedMessage.read_status) {
      // set read status
      this.messages[index].read_status = true;
      this.messageDataService.setMessagesRead(this.selectedMessage.message_id).subscribe();
      this.updateReadCount();
    }

  }

  /**
   * Update the read count on header.
   */
  updateReadCount() {
    let count;
    const readMessages = this.messages.filter(x => x.read_status === false).length;
    if (readMessages > 5) {
      count = '5+';
    } else {
      count = String(readMessages);
    }
    this.messageDataService.sendNumberMessagesToRead(count);
  }

  transformDate(date: string): string {
    return new Date(date).toLocaleString();
  }
}
