import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TasksOwnedComponent} from './tasks-owned.component';
import {TaskDataService} from '../../../shared/task-data.service';
import {CacheMapService} from '../../../shared/cache-map.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TaskOverviewComponent} from '../task-overview/task-overview.component';
import {of} from 'rxjs';
import {TaskTypes} from '../../../models/task-types.enum';

describe('TasksOwnedComponent', () => {
  let component: TasksOwnedComponent;
  let fixture: ComponentFixture<TasksOwnedComponent>;
  let taskDataService: TaskDataService;
  let cache: CacheMapService;
  let locale: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TasksOwnedComponent, TaskOverviewComponent],
      providers: [{provide: TaskDataService, useValue: taskDataService}, {provide: CacheMapService, useValue: cache},
        {provide: Router, useClass: FakeRouter}, {provide: ActivatedRoute, useClass: FakeRoute}, {provide: locale, useValue: locale}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksOwnedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.taskType).toBe(TaskTypes.OWNED);
  });
});

class FakeRouter {
  events = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of(new Event('test')))
  };
}

class FakeRoute {
  queryParams = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({page: 1, entriesPerPage: 20}))
  };
  data = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({tasks: []}))
  };
}
