import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {BackendComponent} from './backend/backend.component';
import {LoginBackendComponent} from './login-backend/login-backend.component';
import {HeaderBackendComponent} from './header-backend/header-backend.component';
import {SidebarBackendComponent} from './sidebar-backend/sidebar-backend.component';
import {EndpointManagementComponent} from './endpoints/endpoint-management/endpoint-management.component';
import {EndpointCreationComponent} from './endpoints/endpoint-creation/endpoint-creation.component';
import {EndpointDetailComponent} from './endpoints/endpoint-detail/endpoint-detail.component';
import {TaskOverviewBackendComponent} from './task-overview-backend/task-overview-backend.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BackendRoutingModule} from './backend-routing.module';
import {NgbTooltipModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {AlertModule} from '../alert/alert.module';

@NgModule({
  declarations: [BackendComponent, LoginBackendComponent, HeaderBackendComponent, SidebarBackendComponent, EndpointManagementComponent, EndpointCreationComponent, EndpointDetailComponent, TaskOverviewBackendComponent],
  imports: [
    CommonModule,
    AlertModule,
    NgbTypeaheadModule,
    NgbTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    BackendRoutingModule
  ],
  exports: [
    RouterModule
  ]
})
export class BackendModule {
}
