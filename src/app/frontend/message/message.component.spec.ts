import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {MessageComponent} from './message.component';
import {MessageDataService} from '../message-data.service';
import {of} from 'rxjs';

describe('MessageComponent', () => {
  const messageSpy = jasmine.createSpyObj('MessageDataService', ['getMessages', 'setMessagesRead', 'sendNumberMessagesToRead']);
  const messages = [{message_date: '2019-02-17T12:30:39+00:00', message_id: 1, text: '1', read_status: false},
    {message_date: '2019-02-17T12:30:39+00:00', message_id: 2, text: '2', read_status: false},
    {message_date: '2019-02-17T12:30:39+00:00', message_id: 3, text: '3', read_status: false},
    {message_date: '2019-02-17T12:30:39+00:00', message_id: 4, text: '4', read_status: false},
    {message_date: '2019-02-17T12:30:39+00:00', message_id: 5, text: '5', read_status: false},
    {message_date: '2019-02-17T12:30:39+00:00', message_id: 6, text: '6', read_status: false}
  ];
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageComponent>;

  beforeEach(async(() => {
    // spy resets
    messageSpy.getMessages.calls.reset();
    messageSpy.setMessagesRead.calls.reset();
    messageSpy.sendNumberMessagesToRead.calls.reset();

    messageSpy.getMessages.and.returnValue(of(messages));
    TestBed.configureTestingModule({
      declarations: [MessageComponent],
      providers: [{provide: MessageDataService, useValue: messageSpy}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(MessageComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(messageSpy.getMessages).toHaveBeenCalled();
    expect(messageSpy.sendNumberMessagesToRead).toHaveBeenCalledWith('5+');
    expect(fixture.nativeElement.querySelector('#message_buttons').children.length).toBe(6);

  }));

  it('should update the readCount when messages get read', fakeAsync(() => {
    messageSpy.setMessagesRead.and.returnValue(of(true));
    tick();
    component.selectMessage(0);
    expect(component.selectedMessage.message_id).toBe(1);
    expect(component.selected).toBeTruthy();
    fixture.detectChanges();
    expect(messageSpy.setMessagesRead).toHaveBeenCalledWith(1);
    expect(messageSpy.sendNumberMessagesToRead).toHaveBeenCalledWith('5');
  }));

  it('should show the text of the selectedMessage', fakeAsync(() => {
    messageSpy.setMessagesRead.and.returnValue(of(true));
    tick();
    component.selectMessage(0);
    tick();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#message_text').textContent).toContain('1');
  }));

  it('should select a message, when it gets clicked on it', fakeAsync(() => {
    messageSpy.setMessagesRead.and.returnValue(of(true));
    tick();
    fixture.nativeElement.querySelector('#message_buttons').children[0].click();
    tick();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#message_text').textContent).toContain('1');
    expect(component.selectedMessage.message_id).toBe(1);
    expect(component.selected).toBeTruthy();
  }));

  it('should append new messages', fakeAsync(() => {
    tick();
    const message = {message_date: '2019-02-17T12:30:39+00:00', message_id: 7, text: '7', read_status: false};
    messageSpy.getMessages.and.returnValue(of([message]));
    tick();
    component.nextPage();
    fixture.detectChanges();
    tick();
    expect(messageSpy.getMessages).toHaveBeenCalled();
    expect(messageSpy.sendNumberMessagesToRead).toHaveBeenCalledWith('5+');
    expect(fixture.nativeElement.querySelector('#message_buttons').children.length).toBe(7);
    expect(component.messages).toContain(message);
  }));

  it('should reload the messages, when reload is pressed', fakeAsync(() => {
    tick();
    const message = {message_date: '2019-02-17T12:30:39+00:00', message_id: 7, text: '7', read_status: false};
    messageSpy.getMessages.and.returnValue(of([message]));
    tick();
    fixture.nativeElement.querySelector('#reload_button').click();
    fixture.detectChanges();
    tick();
    expect(messageSpy.getMessages).toHaveBeenCalledWith(1, 20);
    expect(messageSpy.sendNumberMessagesToRead).toHaveBeenCalledWith('1');
    expect(component.page).toBe(1);
    expect(fixture.nativeElement.querySelector('#message_buttons').children.length).toBe(1);
  }));

});
