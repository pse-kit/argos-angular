import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TasksSigningComponent} from './tasks-signing.component';
import {CacheMapService} from '../../../shared/cache-map.service';
import {TaskDataService} from '../../../shared/task-data.service';
import {TaskOverviewComponent} from '../task-overview/task-overview.component';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TaskTypes} from '../../../models/task-types.enum';

describe('TasksSigningComponent', () => {
  let component: TasksSigningComponent;
  let fixture: ComponentFixture<TasksSigningComponent>;
  let taskDataService: TaskDataService;
  let cache: CacheMapService;
  let locale: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TasksSigningComponent, TaskOverviewComponent],
      providers: [{provide: TaskDataService, useValue: taskDataService}, {provide: CacheMapService, useValue: cache},
        {provide: Router, useClass: FakeRouter}, {provide: ActivatedRoute, useClass: FakeRoute}, {provide: locale, useValue: locale}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksSigningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.taskType).toBe(TaskTypes.TOSIGN);
  });
});

class FakeRouter {
  events = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of(new Event('test')))
  };
}

class FakeRoute {
  queryParams = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({page: 1, entriesPerPage: 20}))
  };
  data = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({tasks: []}))
  };
}
