import {Component, OnInit} from '@angular/core';
import {Preference} from '../../models/preference';
import {UserDataService} from '../../shared/user-data.service';
import {ActivatedRoute} from '@angular/router';

/**
 * Preference Component for frontend.
 */
@Component({
  selector: 'app-preference-frontend',
  templateUrl: './preference-frontend.component.html',
  styleUrls: ['./preference-frontend.component.css']
})
export class PreferenceFrontendComponent implements OnInit {

  preferences: Preference;
  languages = ['de', 'en'];

  constructor(private user_data: UserDataService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: { preferences: Preference }) => {
      this.preferences = data.preferences;
      localStorage.setItem('localeId', this.preferences.language);
    });
  }

  /**
   * Called when the selected language is changed.
   */
  changeLanguage(location = window.location) {
    this.user_data.setLanguagePreference(this.preferences.language).subscribe(x => {
      localStorage.setItem('localeId', this.preferences.language);
      location.replace('/' + this.preferences.language + '/preferences');
      //location.reload(true);
    });
  }

  /*
  * Change the notification settings.
   */
  changeNotification() {
    this.user_data.setNotificationPreference(this.preferences.email_notification).subscribe();
  }


}
