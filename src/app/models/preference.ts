export interface Preference {
  language: string;
  email_notification: boolean;
}
