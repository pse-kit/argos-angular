import {fakeAsync, tick} from '@angular/core/testing';

import {PreferencesResolverService} from './preferences-resolver.service';
import {UserDataService} from '../../shared/user-data.service';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of, throwError} from 'rxjs';
import {Preference} from '../../models/preference';

describe('PreferencesResolverService', () => {
  const userDataSpy = jasmine.createSpyObj('UserDataService', ['getPreferences']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let preferenceResolver: PreferencesResolverService;
  beforeEach(() => {
    userDataSpy.getPreferences.calls.reset();
    routerSpy.navigateByUrl.calls.reset();
    preferenceResolver = new PreferencesResolverService(userDataSpy as UserDataService, routerSpy as Router);
  });

  it('should be created', () => {
    expect(preferenceResolver).toBeTruthy();
  });

  it('should resolve preferences', fakeAsync(() => {
    userDataSpy.getPreferences.and.returnValue(of('value'));
    preferenceResolver.resolve({} as ActivatedRouteSnapshot, {url: '/somewhere-over-the-rainbow'} as RouterStateSnapshot);
    tick();
    expect(userDataSpy.getPreferences).toHaveBeenCalled();
    expect(routerSpy.navigateByUrl).not.toHaveBeenCalled();
  }));

  it('should navigate to where he came from, when the preferences could not be loaded', fakeAsync(() => {
    userDataSpy.getPreferences.and.returnValue(throwError(new Error('some error')));
    tick();
    let result = preferenceResolver.resolve({} as ActivatedRouteSnapshot, {url: '/somewhere-over-the-rainbow'} as RouterStateSnapshot);
    result = (<Observable<Preference>>(<unknown>result));
    result.subscribe();
    tick();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/somewhere-over-the-rainbow');
  }));
});
