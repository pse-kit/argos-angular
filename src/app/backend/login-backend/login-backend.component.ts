import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthorizationService} from '../../auth/authorization.service';
import {Router} from '@angular/router';
import {AlertMessage} from '../../alert/alert-messages';

/**
 * Login for accessing the backend.
 */
@Component({
  selector: 'app-login-backend',
  templateUrl: './login-backend.component.html',
  styleUrls: ['./login-backend.component.css']
})
export class LoginBackendComponent implements OnInit {

  signingForm: FormGroup;
  message: AlertMessage;
  submitted = false;
  loading = false;
  redirectUrl: string;

  constructor(private auth: AuthorizationService, private router: Router) {
  }

  ngOnInit() {
    this.signingForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
      'rememberMe': new FormControl(true)
    });

    // save target url in localstorage
    this.redirectUrl = localStorage.getItem('redirectUrl') || '/admin/endpoints/management';
  }

  /**
   * Submits the login form.
   */
  onSubmit() {
    // reset variables
    this.message = AlertMessage.NOT_SET;

    // Check Form
    if (this.signingForm.valid) {
      this.loading = true;
      this.auth.login(this.signingForm.controls.username.value,
        this.signingForm.controls.password.value, true,
        this.signingForm.controls.rememberMe.value)
        .subscribe((next) => {
            this.router.navigateByUrl(this.redirectUrl);
            localStorage.removeItem('redirectUrl');
            this.loading = false;
          },
          (error) => {
            switch (error.status) {
              case 401: {
                this.message = AlertMessage.LOGIN_FAILED;
                break;
              }
              case 500: {
                this.message = AlertMessage.LDAP_FAILED;
                break;
              }
              default: {
                this.message = AlertMessage.CONNECTION_ERROR;
                break;
              }
            }
            this.loading = false;
          }
        );
    } else {
      // Tried to submit an invalid form
      this.submitted = true;
    }

  }

  changeLanguage(code: string) {
    if (!location.pathname.startsWith('/' + code)) {
      location.replace('/' + code + '/admin/login');
    }
  }

}
