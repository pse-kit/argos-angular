import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

/**
 * Error page component.
 */
@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  /**
   * Redirect user to respective home page.
   */
  redirect() {
    if (this.router.url.includes('admin')) {
      this.router.navigateByUrl('/admin/endpoints/management');
    } else {
      this.router.navigateByUrl('/tasks/owned');
    }
  }
}
