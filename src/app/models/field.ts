export interface Field {
  field_id?: number;
  field_type?: string;
  name?: string;
  value?: any;
  required_status?: boolean;
}
