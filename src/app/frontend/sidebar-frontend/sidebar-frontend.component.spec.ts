import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {SidebarFrontendComponent} from './sidebar-frontend.component';
import {EndpointDataService} from '../../shared/endpoint-data.service';
import {of} from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import {CommonModule} from '@angular/common';

describe('SidebarFrontendComponent', () => {
  let component: SidebarFrontendComponent;
  let fixture: ComponentFixture<SidebarFrontendComponent>;
  const endpointDataSpy = jasmine.createSpyObj('EndpointDataService', ['getEndpoints']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarFrontendComponent],
      imports: [CommonModule, RouterTestingModule],
      providers: [{provide: EndpointDataService, useValue: endpointDataSpy}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    endpointDataSpy.getEndpoints.and.returnValue(of(endpoints));
    fixture = TestBed.createComponent(SidebarFrontendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    tick();
    expect(component).toBeTruthy();
    expect(endpointDataSpy.getEndpoints).toHaveBeenCalledWith(1, 10);
    expect(component.endpoints).toBe(endpoints);
    expect(component.allEndpointsLoaded).toBe(true);
    expect(component.noEndpointsAvailable).toBe(false);
  }));

  it('should show the available endpoints', fakeAsync(() => {
    const list: HTMLDivElement = fixture.nativeElement.querySelector('#endpoints');
    expect(list.children.length).toBe(3);
  }));


  it('should append new loaded endpoints', fakeAsync(() => {
    endpointDataSpy.getEndpoints.and.returnValue(of([endpoints[0]]));
    component.nextPage();
    tick();
    fixture.detectChanges();
    expect(component.endpoints.length).toBe(4);
    expect(component.endpoints).toEqual([...endpoints, endpoints[0]]);
    expect(component.allEndpointsLoaded).toBe(true);
    const list: HTMLDivElement = fixture.nativeElement.querySelector('#endpoints');
    expect(list.children.length).toBe(4);
  }));

  it('should let the endpoints the same, when no more endpoints could get loaded', fakeAsync(() => {
    endpointDataSpy.getEndpoints.and.returnValue(of([]));
    component.nextPage();
    tick();
    fixture.detectChanges();
    expect(component.endpoints.length).toBe(3);
    expect(component.endpoints).toEqual(endpoints);
    expect(component.allEndpointsLoaded).toBe(true);
    const list: HTMLDivElement = fixture.nativeElement.querySelector('#endpoints');
    expect(list.children.length).toBe(3);
  }));

  it('should show a button to load more endpoints, if more are available', fakeAsync(() => {
    component.allEndpointsLoaded = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#load_more_endpoints')).toBeDefined();
  }));

  it('should show a error Message when no endpoints available', fakeAsync(() => {
    endpointDataSpy.getEndpoints.and.returnValue(of([]));
    fixture = TestBed.createComponent(SidebarFrontendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const list: HTMLDivElement = fixture.nativeElement.querySelector('#endpoints');
    expect(list.children.length).toBe(1);
    expect(list.children[0].id).toBe('no_endpoints_available');
  }));
});

const endpoints = [{
  'additional_access_insert_before_status': true,
  'allow_additional_accesses_status': true,
  'deletable_status': false,
  'description': 'Schnell und einfach einen neuen Firmenwagen beantragen!',
  'email_notification_status': true,
  'endpoint_id': 1,
  'fields': [{'field_id': 1, 'field_type': 'textfield', 'name': 'Fahrleistung', 'required_status': true}, {
    'field_id': 2,
    'field_type': 'datetime',
    'name': 'Gew\u00fcnschtes Auslieferungsdatum',
    'required_status': true
  }, {'field_id': 3, 'field_type': 'textfield', 'name': 'Anmerkungen', 'required_status': false}],
  'given_accesses': [{
    'access_order': 0,
    'group': {'description': 'Beschreibung Vorstand', 'group_id': 1, 'name': 'Vorstand'},
    'user': null
  }],
  'instantiable_status': true,
  'name': 'Firmenwagen',
  'sequential_status': false,
  'signature_type': 'checkbox'
}, {
  'additional_access_insert_before_status': false,
  'allow_additional_accesses_status': true,
  'deletable_status': true,
  'description': 'Komplexe Beschreibung',
  'email_notification_status': true,
  'endpoint_id': 6,
  'fields': [{'field_id': 17, 'field_type': 'datetime', 'name': 'Komplexes Datum', 'required_status': true}, {
    'field_id': 18,
    'field_type': 'file',
    'name': 'Komplexe Datei',
    'required_status': true
  }, {'field_id': 19, 'field_type': 'textfield', 'name': 'irgendwas', 'required_status': false}, {
    'field_id': 20,
    'field_type': 'checkbox',
    'name': 'irgendeine Checkbox',
    'required_status': false
  }],
  'given_accesses': [{
    'access_order': 0,
    'group': null,
    'user': {'email_address': 'N-Schuler@gmx.de', 'name': 'Nicolas Schuler', 'user_id': 3}
  }, {
    'access_order': 1,
    'group': null,
    'user': {'email_address': 'wendy.yi@web.de', 'name': 'Wendy Yi', 'user_id': 4}
  }, {
    'access_order': 2,
    'group': null,
    'user': {'email_address': 'julius.haecker@web.de', 'name': 'Julius H\u00e4cker', 'user_id': 2}
  }, {
    'access_order': 3,
    'group': null,
    'user': {'email_address': 'noah.ares@yahoo.de', 'name': 'Noah Wahl', 'user_id': 5}
  }, {'access_order': 4, 'group': null, 'user': {'email_address': 'moritzleitner@gmail.com', 'name': 'Moritz Leitner', 'user_id': 1}}],
  'instantiable_status': true,
  'name': 'Komplex',
  'sequential_status': true,
  'signature_type': 'buttonAfterDownload'
}, {
  'additional_access_insert_before_status': true,
  'allow_additional_accesses_status': true,
  'deletable_status': false,
  'description': 'Muss f\u00fcr den Urlaub eingereicht werden.',
  'email_notification_status': true,
  'endpoint_id': 5,
  'fields': [{'field_id': 13, 'field_type': 'datetime', 'name': 'Beginn', 'required_status': true}, {
    'field_id': 14,
    'field_type': 'datetime',
    'name': 'Ende',
    'required_status': true
  }, {'field_id': 15, 'field_type': 'textfield', 'name': 'Bemerkung', 'required_status': false}, {
    'field_id': 16,
    'field_type': 'checkbox',
    'name': 'Vertretung vorhanden?',
    'required_status': true
  }],
  'given_accesses': [{'access_order': 0, 'group': {'description': null, 'group_id': 6, 'name': 'Personalabteilung'}, 'user': null}],
  'instantiable_status': true,
  'name': 'Urlaubsantrag',
  'sequential_status': true,
  'signature_type': 'button'
}];
