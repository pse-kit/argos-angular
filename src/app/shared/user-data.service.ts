import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Preference} from '../models/preference';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../models/user';
import {Group} from '../models/group';
import {Clearable} from './clear.service';
import {RouteGenerator} from './route-generator';

const ignoreLoadingBar = new HttpHeaders('ignoreLoadingBar: ');


@Injectable({
  providedIn: 'root'
})
export class UserDataService implements Clearable {

  currentUser: User = null;

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Requests the webservice to get preferences for the authorized user.
   */
  getPreferences(): Observable<Preference> {
    const languages = this.httpClient.get(RouteGenerator.getPreferencesLanguageRoute(), {observe: 'response'});
    const notifications = this.httpClient.get(RouteGenerator.getPreferencesNotificationsRoute(),
      {observe: 'response'});

    return forkJoin([languages, notifications]).pipe(map(results => {
      return <Preference>{'language': results[0].body['language'], 'email_notification': results[1].body['notification_status']};
    }));
  }


  /**
   * Request to set the language setting for the authorized user.
   */
  setLanguagePreference(language: string) {
    return this.httpClient.put(RouteGenerator.getPreferencesLanguageRoute(),
      {'language': language});
  }

  /**
   * Request to set the Notification setting for the authorized user.
   */
  setNotificationPreference(notification_status: boolean) {
    return this.httpClient.put(RouteGenerator.getPreferencesNotificationsRoute(),
      {'notification_status': notification_status});

  }

  /**
   * Get user suggestions to  search term from the webservice.
   */
  userSuggestions(searchTerm: string) {
    const params = new HttpParams().set('search', searchTerm);
    return this.httpClient.get<User[]>(RouteGenerator.getUsersSearchRoute(), {
      params,
      headers: ignoreLoadingBar
    }).pipe(map(x => x['users']));
  }

  /**
   * Get group suggestions to search term from the webservice.
   */
  groupSuggestions(searchTerm: string) {
    const params = new HttpParams().set('search', searchTerm);
    return this.httpClient.get<Group[]>(RouteGenerator.getGroupsSearchRoute(), {
      params,
      headers: ignoreLoadingBar
    }).pipe(map(x => x['groups']));
  }

  /**
   * Get information to currently authorized user from the webservice.
   */
  async getCurrentUser() {
    if (this.currentUser === null) {
      this.currentUser = await this.httpClient.get<User>(RouteGenerator.getUsersCurrentRoute()).toPromise();
      return this.currentUser;
    } else {
      return this.currentUser;
    }
  }

  /**
   * Clear the currently set user.
   */
  clear() {
    this.currentUser = null;
  }

}
