import {Field} from './field';
import {GivenAccess} from './given-access';

export interface Endpoint {
  endpoint_id?: number;
  name: string;
  description: string;
  signature_type: string;
  sequential_status: boolean;
  deletable_status: boolean;
  fields: Field[];
  instantiable_status?: boolean;
  email_notification_status: boolean;
  allow_additional_accesses_status: boolean;
  given_accesses: GivenAccess[];
  active_tasks?: number;
  additional_access_insert_before_status?: boolean;
}
