import {Component, OnInit} from '@angular/core';
import {EndpointDataService} from '../../../shared/endpoint-data.service';
import {Endpoint} from '../../../models/endpoint';
import {CacheMapService} from '../../../shared/cache-map.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertMessage} from '../../../alert/alert-messages';
import {DE_Confirm, EN_Confirm} from '../../../models/confirm.language';

/**
 * Component for managing endpoints.
 */
@Component({
  selector: 'app-endpoint-management',
  templateUrl: './endpoint-management.component.html',
  styleUrls: ['./endpoint-management.component.css']
})
export class EndpointManagementComponent implements OnInit {

  page = 1;
  entriesPerPage = 20;
  nextEnabled = true;
  endpoints: Endpoint[] = [];
  subscription;
  message: AlertMessage;

  constructor(private endpointDataService: EndpointDataService, private cache: CacheMapService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      this.page = +params['page'] || 1;
      this.entriesPerPage = +params['entriesPerPage'] || 20;
    });
    this.loadEndpoints();
  }

  /**
   * Loads next endpoint page.
   */
  nextPage() {
    this.page++;
    this.loadEndpoints();
  }

  /**
   * Loads the previous page.
   */
  previousPage() {
    this.page--;
    this.loadEndpoints();
    this.nextEnabled = true;
  }

  /**
   * Deleting an endpoint. Sends a request to the webservice if admin confirms deletion.
   */
  deleteEndpoint(id: number) {
    const index = this.endpoints.findIndex(endpoint => {
      return endpoint.endpoint_id === id;
    });
    if (index > -1) {
      if (this.endpoints[index].active_tasks !== 0) {
        if (localStorage.getItem('localeId') === 'en' ?
          confirm(EN_Confirm.DELETE_ENDPOINT_ACTIVE_TASKS) :
          confirm(DE_Confirm.DELETE_ENDPOINT_ACTIVE_TASKS)) {
          this.reloadAfterDelete(id);
        }
      } else {
        if (localStorage.getItem('localeId') === 'en' ?
          confirm(EN_Confirm.DELETE_ENDPOINT) :
          confirm(DE_Confirm.DELETE_ENDPOINT)) {
          this.reloadAfterDelete(id);
        }
      }
    } else {
      this.message = AlertMessage.SOMETHING_WENT_WRONG;
    }
  }

  /**
   * Reloads the endpoints.
   */
  reload() {
    this.message = AlertMessage.NOT_SET;
    this.cache.flush('endpoints');
    this.loadEndpoints();
  }

  /**
   * Loads new endpoints.
   */
  loadEndpoints() {
    this.endpointDataService.getEndpoints(this.page, this.entriesPerPage).subscribe(results => {
      this.nextEnabled = results.length === this.entriesPerPage;
      this.endpoints = results;
    }, err => {
      this.message = AlertMessage.SOMETHING_WENT_WRONG;
    });
  }

  /**
   * Subroutine for reloading the endpoints.
   */
  reloadAfterDelete(id: number) {
    this.endpointDataService.deleteEndpoint(id).subscribe(results => {
      if (results) {
        this.reload();
      }
    }, err => {
      this.message = AlertMessage.SOMETHING_WENT_WRONG;
    });
  }

  /**
   * Redirect to endpoint creation shown if no endpoint is available.
   */
  redirectCreation() {
    this.router.navigateByUrl('/admin/endpoints/creation');
  }
}
