export interface User {
  user_id: number;
  name: string;
  username?: string;
  email_address?: string;
  password?: string;
}
