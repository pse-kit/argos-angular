export enum TaskTypes {
  ACTIVE = 'ACTIVE',
  OWNED = 'OWNED',
  TOSIGN = 'TOSIGN',
  ARCHIVED = 'ARCHIVED'
}
