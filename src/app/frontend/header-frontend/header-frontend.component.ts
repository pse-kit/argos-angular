import {Component, OnInit} from '@angular/core';
import {AuthorizationService} from '../../auth/authorization.service';
import {Router} from '@angular/router';
import {MessageDataService} from '../message-data.service';
import {User} from '../../models/user';
import {UserDataService} from '../../shared/user-data.service';
import {ClearService} from '../../shared/clear.service';

/*
 * Header for the frontend view.
 */
@Component({
  selector: 'app-header-frontend',
  templateUrl: './header-frontend.component.html',
  styleUrls: ['./header-frontend.component.css']
})
export class HeaderFrontendComponent implements OnInit {

  currentUser: User;
  messagesCount: string;

  constructor(private auth: AuthorizationService, private router: Router,
              private messageData: MessageDataService, private userDataService: UserDataService, private clearService: ClearService) {
  }

  ngOnInit() {
    this.messageData.getMessagesToReadCount().subscribe(next => {
      this.messagesCount = next;
    });

    this.userDataService.getCurrentUser().then(user => {
      this.currentUser = user;
    });
  }

  /**
   * Called when logout button is clicked.
   */
  logout() {
    this.auth.logout();
    this.clearService.clearAllServices();
    this.router.navigateByUrl('/login');
  }

}
