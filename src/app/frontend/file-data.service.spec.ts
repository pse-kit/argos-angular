import {fakeAsync, tick} from '@angular/core/testing';

import {FileDataService} from './file-data.service';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {of, throwError} from 'rxjs';
import {RouteGenerator} from '../shared/route-generator';

describe('FileDataService', () => {
  const httpSpy = jasmine.createSpyObj('HttpClient', ['get']);
  let fileDataService: FileDataService;

  beforeEach(() => {
    fileDataService = new FileDataService(httpSpy as HttpClient);
  });

  it('should be created', () => {
    expect(fileDataService).toBeTruthy();
  });

  it('should extract the link out of the url from the response', fakeAsync(() => {
    httpSpy.get.and.returnValue(of(new HttpResponse({body: {link: 'some-url'}})));
    fileDataService.getFilesByUrl(1).subscribe(value => expect(value).toBe(RouteGenerator.getDownloadRoute('some-url'))
      , error => fail());
    tick();
  }));

  it('should throw an error, to the caller', fakeAsync(() => {
    httpSpy.get.and.returnValue(throwError(new HttpErrorResponse({status: 400})));
    fileDataService.getFilesByUrl(1).subscribe(value => fail(), error => expect(error).toBeDefined());
    tick();
  }));
});
