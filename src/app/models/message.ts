export interface Message {
  recipient_id?: number;
  message_id?: number;
  message_date: string;
  text: string;
  read_status: boolean;
}
