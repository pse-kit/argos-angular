import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Endpoint} from '../models/endpoint';
import {map} from 'rxjs/operators';
import {RouteGenerator} from './route-generator';

@Injectable({
  providedIn: 'root'
})
export class EndpointDataService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Gets an endpoint from the webservice.
   */
  getEndpoint(id: number) {
    return this.httpClient.get<Endpoint>(RouteGenerator.getEndpointsIdRoute(id));
  }

  /**
   * Gets a list of endpoints from the webservice.
   */
  getEndpoints(page: number, entriesPerPage: number) {
    const params = new HttpParams().set('limit', String(entriesPerPage))
      .append('page', String(page));
    return this.httpClient.get<Endpoint[]>(RouteGenerator.getEndpointsRoute(), {params}).pipe(map(x => {
      return x['endpoints'];
    }));
  }

  /**
   * Request to create an endpoint.
   */
  createEndpoint(endpoint: Endpoint) {
    return this.httpClient.post(RouteGenerator.getEndpointsRoute(), endpoint);
  }

  /**
   * Request to delete an endpoint.
   */
  deleteEndpoint(id: number) {
    return this.httpClient.delete(RouteGenerator.getEndpointsIdRoute(id), {observe: 'response'}).pipe(map((resp) => {
      return resp.status === 200;
    }));
  }
}
