import {EndpointDataService} from './endpoint-data.service';
import {HttpClient} from '@angular/common/http';

describe('EndpointDataService', () => {
  const httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);
  let endpointService: EndpointDataService;
  beforeEach(() => {
    endpointService = new EndpointDataService(httpSpy as HttpClient);
  });

  it('should be created', () => {
    expect(endpointService).toBeTruthy();
  });
});
