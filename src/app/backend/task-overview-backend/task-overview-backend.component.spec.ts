import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskOverviewBackendComponent} from './task-overview-backend.component';
import {ActivatedRoute, Router} from '@angular/router';
import {CacheMapService} from '../../shared/cache-map.service';
import {TaskDataService} from '../../shared/task-data.service';
import {of} from 'rxjs';

describe('TaskOverviewBackendComponent', () => {
  let component: TaskOverviewBackendComponent;
  let fixture: ComponentFixture<TaskOverviewBackendComponent>;
  let fakeTaskService: FakeTaskService;
  let fakeCache: FakeCache;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaskOverviewBackendComponent],
      providers: [{provide: TaskDataService, useClass: FakeTaskService}, {provide: CacheMapService, useClass: FakeCache},
        {provide: Router, useValue: routerSpy}, {provide: ActivatedRoute, useClass: FakeRoute}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(TaskOverviewBackendComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      fakeTaskService = TestBed.get(TaskDataService);
      fakeCache = TestBed.get(CacheMapService);
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate flawlessly between pages', () => {
    component.nextPage();
    component.nextPage();
    expect(component.nextEnabled).toBe(false);
    component.previousPage();
    expect(component.nextEnabled).toBe(true);
  });
});

class FakeTaskService {

  getTasks(page: number, entriesPerPage: number) {
    return of([]);
  }
}

class FakeCache {

  flush(type: string) {
    return true;
  }
}

class FakeRoute {
  queryParams = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({page: 1, entriesPerPage: 20}))
  };
}
