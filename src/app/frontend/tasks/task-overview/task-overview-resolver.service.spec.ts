import {fakeAsync, tick} from '@angular/core/testing';

import {TaskOverviewResolverService} from './task-overview-resolver.service';
import {TaskDataService} from '../../../shared/task-data.service';
import {ActivatedRouteSnapshot, ParamMap, Router, RouterStateSnapshot, UrlSegment} from '@angular/router';
import {Observable, of, throwError} from 'rxjs';
import {Task} from '../../../models/task';

describe('TaskOverviewResolverService', () => {
  const taskDataSpy = jasmine.createSpyObj('TaskDataService', ['getTasks']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let taskOverviewResolver: TaskOverviewResolverService;
  beforeEach(() => {
    taskDataSpy.getTasks.calls.reset();
    routerSpy.navigateByUrl.calls.reset();
    taskOverviewResolver = new TaskOverviewResolverService(taskDataSpy as TaskDataService, routerSpy as Router);
  });

  it('should be created', () => {
    expect(taskOverviewResolver).toBeTruthy();
  });

  it('should resolve an set attributes right', fakeAsync(() => {
    taskDataSpy.getTasks.and.returnValue(of([{task_id: 1, name: 'test', endpoint_id: 1}]));
    const activeRoute: ActivatedRouteSnapshot = {
      queryParams: {
        page: 1,
        entriesPerPage: 20
      },
      url: [new UrlSegment('/test', {})],
      params: [],
      fragment: 'test',
      data: [],
      outlet: 'test',
      component: null,
      routeConfig: null,
      root: null,
      parent: null,
      firstChild: null,
      children: [],
      pathFromRoot: [],
      queryParamMap: new MockParamMap(),
      paramMap: new MockParamMap()
    };
    let tasks;
    let result = taskOverviewResolver.resolve(activeRoute as ActivatedRouteSnapshot, {url: '/test/archived'} as RouterStateSnapshot);
    result = (<Observable<Task[]>>(<unknown>result));
    result.subscribe(val => {
      tasks = val;
      expect(tasks).toEqual([{task_id: 1, name: 'test', endpoint_id: 1}]);
    });
    tick();
    expect(taskDataSpy.getTasks).toHaveBeenCalled();
  }));

  it('should navigate on error', fakeAsync(() => {
    taskDataSpy.getTasks.and.returnValue(throwError(new Error()));
    const activeRoute: ActivatedRouteSnapshot = {
      queryParams: {
        page: 1,
        entriesPerPage: 20
      },
      url: [new UrlSegment('/test', {})],
      params: [],
      fragment: 'test',
      data: [],
      outlet: 'test',
      component: null,
      routeConfig: null,
      root: null,
      parent: null,
      firstChild: null,
      children: [],
      pathFromRoot: [],
      queryParamMap: new MockParamMap(),
      paramMap: new MockParamMap()
    };
    let tasks;
    let result = taskOverviewResolver.resolve(activeRoute as ActivatedRouteSnapshot, {url: '/test/archived'} as RouterStateSnapshot);
    result = (<Observable<Task[]>>(<unknown>result));
    result.subscribe(val => {
      tasks = val;
    });
    tick();
    expect(taskDataSpy.getTasks).toHaveBeenCalled();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/test/archived');
  }));
});

class MockParamMap implements ParamMap {
  keys;

  has(name: string) {
    return true;
  }

  get(name: string) {
    return 'test';
  }

  getAll(name: string) {
    return this.keys;
  }
}


