import {Component, Input, OnInit} from '@angular/core';
import {AlertMessage} from './alert-messages';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  alertMessages = AlertMessage;
  @Input() message: AlertMessage;

  constructor() {
  }

  ngOnInit() {
  }

}
