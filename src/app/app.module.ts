import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FrontendModule} from './frontend/frontend.module';
import {BackendModule} from './backend/backend.module';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizationService} from './auth/authorization.service';
import {TokenInterceptorService} from './auth/token.interceptor';
import {CacheMapService} from './shared/cache-map.service';
import {CachingInterceptor} from './shared/cache.interceptor';
import {Cache} from './shared/cache';
import {ErrorsModule} from './errors/errors.module';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {FlatpickrModule} from 'angularx-flatpickr';

const appRoutes: Routes = [];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LoadingBarHttpClientModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    ErrorsModule,
    FlatpickrModule.forRoot(),
    FrontendModule,
    BackendModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true},
    AuthorizationService,
    {provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true},
    CacheMapService,
    {provide: Cache, useClass: CacheMapService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
