import {Component} from '@angular/core';
import {TaskDataService} from '../../../shared/task-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CacheMapService} from '../../../shared/cache-map.service';
import {TaskOverviewComponent} from '../task-overview/task-overview.component';
import {TaskTypes} from '../../../models/task-types.enum';


@Component({
  selector: 'app-tasks-history',
  templateUrl: './tasks-history.component.html',
  styleUrls: ['./tasks-history.component.css']
})
export class TasksHistoryComponent extends TaskOverviewComponent {

  constructor(protected taskDataService: TaskDataService, protected cache: CacheMapService, protected router: Router, protected activatedRoute: ActivatedRoute) {
    super(taskDataService, cache, router, activatedRoute);
    this.taskType = TaskTypes.ARCHIVED;
  }

  deleteTask(id: number) {
    const index = this.tasks.findIndex(task => {
      return task.task_id === id;
    });
    this.taskDataService.deleteTask(id).subscribe(result => {
      this.tasks.splice(index, 1);
      if (this.tasks.length % this.entriesPerPage === this.entriesPerPage - 1) {
        this.nextEnabled = false;
      }
    });
  }

  transformDate(date: string): string {
    return new Date(date).toLocaleString();
  }

}
