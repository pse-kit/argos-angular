import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthorizationService} from '../../auth/authorization.service';
import {Router} from '@angular/router';
import {AlertMessage} from '../../alert/alert-messages';

/**
 * Login component for frontend.
 */
@Component({
  selector: 'app-login-frontend',
  templateUrl: './login-frontend.component.html',
  styleUrls: ['./login-frontend.component.css']
})
export class LoginFrontendComponent implements OnInit {

  signingForm: FormGroup;
  message: AlertMessage;
  submitted = false;
  loading = false;
  redirectUrl: string;

  constructor(private auth: AuthorizationService, private router: Router) {
  }

  ngOnInit() {
    this.signingForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
      'rememberMe': new FormControl(true)
    });

    this.redirectUrl = localStorage.getItem('redirectUrl') || '/tasks/owned';
  }

  /**
   *  Called when submitting the signingForm.
   */
  onSubmit() {
    // Reset variables
    this.message = AlertMessage.NOT_SET;

    if (this.signingForm.valid) {
      this.loading = true;
      this.auth.login(this.signingForm.controls.username.value,
        this.signingForm.controls.password.value, false,
        this.signingForm.controls.rememberMe.value)
        .subscribe((next) => {
            localStorage.removeItem('redirectUrl');
            this.loading = false;
            this.router.navigateByUrl(this.redirectUrl);
          },
          (error) => {
            switch (error.status) {
              case 401: {
                this.message = AlertMessage.LOGIN_FAILED;
                break;
              }
              case 500: {
                this.message = AlertMessage.LDAP_FAILED;
                break;
              }
              default: {
                this.message = AlertMessage.CONNECTION_ERROR;
                break;
              }
            }
            this.loading = false;
          }
        );
    } else {
      // Tried to submit an invalid form
      this.submitted = true;
    }

  }

}
