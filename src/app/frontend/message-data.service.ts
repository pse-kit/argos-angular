import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Message} from '../models/message';
import {map, retry} from 'rxjs/operators';
import {Clearable} from '../shared/clear.service';
import {RouteGenerator} from '../shared/route-generator';

@Injectable({
  providedIn: 'root'
})
export class MessageDataService implements Clearable {
  messagesToReadCount: Subject<string>;

  constructor(private httpClient: HttpClient) {
    this.messagesToReadCount = new Subject();
  }

  /**
   * Get messages from the webservice.
   */
  getMessages(page: number, entriesPerPage: number): Observable<Message[]> {
    const params = new HttpParams().set('limit', String(entriesPerPage)).append('page', String(page));
    return this.httpClient.get<Message[]>(RouteGenerator.getMessagesRoute(), {params: params}).pipe(map(x => {
      return x['messages'];
    }));
  }

  /**
   * Set message with id read.
   */
  setMessagesRead(id: number) {
    return this.httpClient.put(RouteGenerator.getMessagesStatusRoute(id), {}, {
      observe: 'response',
      headers: new HttpHeaders('ignoreLoadingBar: ')
    })
      .pipe(retry(2), map(response => {
        return response.status === 200;
      }));
  }

  /**
   * Notify Observer on updated message read count.
   */
  sendNumberMessagesToRead(count: string) {
    this.messagesToReadCount.next(count);
  }

  /**
   * Get message read count as an observable.
   */
  getMessagesToReadCount() {
    return this.messagesToReadCount.asObservable();
  }

  /**
   * Resets the messageToReadCount.
   */
  clear() {
    this.messagesToReadCount = new Subject();
  }
}
