import {User} from './user';
import {Group} from './group';

export interface Access {
  access_status?: string;
  event_date?: string;
  access_order?: number;
  user_id?: number;
  group_id?: number;
  user?: User;
  group?: Group;
}
