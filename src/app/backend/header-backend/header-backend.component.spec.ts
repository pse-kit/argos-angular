import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderBackendComponent} from './header-backend.component';
import {ClearService} from '../../shared/clear.service';
import {AuthorizationService} from '../../auth/authorization.service';
import {Router} from '@angular/router';

describe('HeaderBackendComponent', () => {
  let component: HeaderBackendComponent;
  let fixture: ComponentFixture<HeaderBackendComponent>;
  let fakeAuth: FakeAuthorizationService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderBackendComponent],
      providers: [{provide: AuthorizationService, useClass: FakeAuthorizationService},
        {provide: ClearService, useClass: FakeClearService}, {provide: Router, useValue: routerSpy}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(HeaderBackendComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      fakeAuth = TestBed.get(AuthorizationService);
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should logout correctly and redirect to admin login', () => {
    component.logout();
    const spy = routerSpy.navigateByUrl as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];
    expect(navArgs).toBe('/admin/login');
  });

  // TODO: language code switch so far not testable
});

class FakeAuthorizationService {

  logout() {
    return true;
  }
}

class FakeClearService {

  clearAllServices() {
    return true;
  }
}
