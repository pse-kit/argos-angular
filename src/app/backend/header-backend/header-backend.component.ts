import {Component, OnInit} from '@angular/core';
import {AuthorizationService} from '../../auth/authorization.service';
import {Router} from '@angular/router';
import {ClearService} from '../../shared/clear.service';

/**
 * Header for the backend view.
 */
@Component({
  selector: 'app-header-backend',
  templateUrl: './header-backend.component.html',
  styleUrls: ['./header-backend.component.css']
})
export class HeaderBackendComponent implements OnInit {

  constructor(private auth: AuthorizationService, private router: Router, private clearService: ClearService) {
  }

  ngOnInit() {
  }

  /**
   * Called when logout button is clicked.
   */
  logout() {
    this.auth.logout();
    this.clearService.clearAllServices();
    this.router.navigateByUrl('/admin/login');
  }

  changeLanguage(code: string) {
    if (!location.pathname.includes('/' + code + '/')) {
      location.replace('/' + code + this.router.url);
    }
  }
}
