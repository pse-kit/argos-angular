import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, of} from 'rxjs';
import {FormArray, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModule} from 'src/app/alert/alert.module';
import {HttpResponse} from '@angular/common/http';

import {TaskCreationComponent} from './task-creation.component';
import {TaskDataService} from 'src/app/shared/task-data.service';
import {EndpointDataService} from 'src/app/shared/endpoint-data.service';
import {UserDataService} from 'src/app/shared/user-data.service';
import {Task} from 'src/app/models/task';
import {AlertMessage} from '../../../alert/alert-messages';
import {NgbTooltipModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import {FlatpickrModule} from 'angularx-flatpickr';
import {Endpoint} from 'src/app/models/endpoint';

describe('TaskCreationComponent', () => {
  let component: TaskCreationComponent;
  let fixture: ComponentFixture<TaskCreationComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const endpointSpy = jasmine.createSpyObj('EndpointDataService', ['getEndpoint']);

  let taskData: TaskDataService;
  let endpointData: EndpointDataService;
  let userData: UserDataService;


  let mockEndpoint: Endpoint = {
    endpoint_id: 1,
    name: 'MockingJay',
    description: 'District 12',
    signature_type: 'button',
    sequential_status: true,
    deletable_status: true,
    fields: [{field_id: 1, field_type: 'textfield', name: 'MockTextfield', required_status: true},
      {field_id: 2, field_type: 'datetime', name: 'MockDate', required_status: false}
    ],
    instantiable_status: true,
    email_notification_status: true,
    allow_additional_accesses_status: true,
    given_accesses: [{access_order: 0, group: null, user: {user_id: 1, name: 'MockUser'}},
      {access_order: 1, group: {group_id: 1, name: 'MockGroup'}, user: null}
    ],
    active_tasks: 10,
    additional_access_insert_before_status: false
  };

  beforeEach(fakeAsync(() => {
    endpointSpy.getEndpoint.and.returnValue(of(mockEndpoint));
    TestBed.configureTestingModule({
      declarations: [TaskCreationComponent],
      imports: [FormsModule, ReactiveFormsModule, AlertModule, NgbTooltipModule, NgbTypeaheadModule, FlatpickrModule.forRoot()],
      providers: [{provide: EndpointDataService, useValue: endpointSpy as EndpointDataService},
        {provide: TaskDataService, useClass: FakeTaskService},
        {provide: UserDataService, useClass: FakeUserService},
        {provide: ActivatedRoute, useClass: FakeRoute},
        {provide: Router, useValue: routerSpy}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(TaskCreationComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      taskData = TestBed.get(TaskDataService);
      endpointData = TestBed.get(EndpointDataService);
      userData = TestBed.get(UserDataService);

      tick();
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.endpoint).toBeDefined();
    expect(component.endpoint.name).toBe('MockingJay');
  });

  it('add fields to form', () => {
    endpointData.getEndpoint(1).subscribe(result => {
      let i = 0;
      for (let formField of (<FormArray>component.taskCreationForm.controls.fields).controls) {
        expect(formField.get('field_id').value)
          .toBe(mockEndpoint.fields[i].field_id);
        expect(formField.get('field_type').value)
          .toBe(mockEndpoint.fields[i].field_type);
        expect(formField.get('name').value)
          .toBe(mockEndpoint.fields[i].name);
        expect(formField.get('required_status').value)
          .toBe(mockEndpoint.fields[i].required_status);

        i++;
      }
    });
  });

  it('add additional user access', () => {
    let sig = {item: {email_address: 'mock@mocking.de', name: 'MockUser', user_id: 1}};
    component.addSignature(sig);
    expect(component.additionalAccesses).toEqual([{signee: sig.item, isUser: true}]);
  });

  it('remove additional user access', () => {
    let sig = {item: {email_address: 'mock@mocking.de', name: 'MockUser', user_id: 1}};
    component.additionalAccesses = [{signee: sig.item, isUser: true}];
    component.removeAdditionalAccess(0);
    expect(component.additionalAccesses).toEqual([]);
  });

  it('check disable with not null endpoint', () => {
    (<FormArray>component.taskCreationForm.controls.fields).at(0).get('value').setValue('MockContent');
    component.taskCreationForm.controls.name.setValue('MockName');
    expect(component.checkDisable()).toBe(false);
  });

  it('check disable with null endpoint', () => {
    component.endpoint = null;
    expect(component.checkDisable()).toBe(true);
  });

  it('can deactivate with not dirty form', () => {
    expect(component.canDeactivate()).toBe(true);
  });

  it('can deactivate with dirty form', () => {
    (<FormArray>component.taskCreationForm.controls.fields).at(0).get('value').markAsDirty();
    expect((<FormArray>component.taskCreationForm.controls.fields).at(0).get('value').dirty).toBeTruthy();
    spyOn(window, 'confirm').and.returnValue(true);
    expect(component.canDeactivate()).toBeTruthy();
  });

  it('cancel creation', () => {
    (<FormArray>component.taskCreationForm.controls.fields).at(0).get('value').setValue('MockContent');
    component.taskCreationForm.controls.name.setValue('MockName');
    component.cancelCreation();
    expect((<FormArray>component.taskCreationForm.controls.fields).at(0).get('value').value).toEqual(null);
    expect(component.taskCreationForm.controls.name.value).toBe('');
  });

  it('add files', () => {
    const blob = new Blob([''], {type: 'text/html'});
    blob['lastModifiedDate'] = '';
    blob['name'] = 'filename';
    const file = <File>blob;
    const fileList: FileList = {
      0: file,
      1: file,
      length: 2,
      item: (index: number) => file
    };

    component.addFiles(fileList);
    expect(component.formFiles).toEqual([file, file]);
    expect(component.requiredFile).toBeTruthy();
  });

  it('submit task', () => {
    component.taskCreationForm.controls.name.setValue('MockName');
    (<FormArray>component.taskCreationForm.controls.fields).at(0).get('value').setValue('MockContent');

    component.onSubmit();

    expect(component.message).toBe(AlertMessage.TASK_CREATED);
    expect(component.taskCreationForm.controls.name.value).toBe('');
    expect((<FormArray>component.taskCreationForm.controls.fields).at(0).value)
      .toEqual({field_id: 1, field_type: 'textfield', name: 'MockTextfield', value: null, required_status: true});
  });


});


class FakeEndpointService {
  getEndpoint(id: number) {
    return of({
      endpoint_id: 1,
      name: 'MockingJay',
      description: 'District 12',
      signature_type: 'button',
      sequential_status: true,
      deletable_status: true,
      fields: [{field_id: 1, field_type: 'textfield', name: 'MockTextfield', required_status: true},
        {field_id: 2, field_type: 'datetime', name: 'MockDate', required_status: false}
      ],
      instantiable_status: true,
      email_notification_status: true,
      allow_additional_accesses_status: true,
      given_accesses: [{access_order: 0, group: null, user: {user_id: 1, name: 'MockUser'}},
        {access_order: 1, group: {group_id: 1, name: 'MockGroup'}, user: null}
      ],
      active_tasks: 10,
      additional_access_insert_before_status: false
    });
  };
}

class FakeUserService {
  async getCurrentUser() {
    return of({user_id: 1, name: 'MockUser'});
  }

  userSuggestions(searchTerm: string) {
    return of({user_id: 1, name: 'MockUser'});
  }

  groupSuggestions(searchTerm: string) {
    return of({group_id: 1, name: 'MockGroup'});
  }
}

class FakeTaskService {
  createNewTask(task: Task, files: File[]) {
    //const response = new HttpResponse({status: 200});
    return new BehaviorSubject(new HttpResponse({status: 200}));
  }
}

class FakeRoute {
  params = new BehaviorSubject({id: '1'});
}
