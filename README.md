# Argos - Angular
## Installation
Requirements: [NPM from Nodejs](https://nodejs.org/en/)

To install this project, navigate to its root directory with your terminal and run:
```console
npm install
```
Note: If you use an IDE like WebStorm it will offer to do this for you the first time you open this project.  

## Connect to the argos webservice
To connect to the argos webservice on a different url, you need to make changes in the following file:
```console
In src/app/shared/route-generator.ts edit the ROOT_PATH and DOMAIN in the enum Routes.
```
Note: You absolutely need to store the different language builds on their respective subdirectory
on your webserver (e.g. the content of dist/de needs to be stored in example.com/de).
Also your webserver needs to be configured to redirect all requests to the index.html in their respective subdirectory.

## Development Server

Run `ng serve` to start the development server. Then open your browser at `http://localhost:4200/`. The app will refresh everytime you make changes in the source code.

## Build

Run `ng build` to build the project. The created artifacts will be stored in `dist/`. Use the `--prod` flag for a production build. 
To build this project in german and english run `npm run build-i18n`. The artifacts will now be available in the `de/` and `en/` subdirectories of `dist/`. 

## Tests

Run `ng test` for unit tests with [Karma](https://karma-runner.github.io).

Run `npm run test -- --browsers ChromeHeadlessNoSandbox --watch=false --code-coverage -` for generating the code coverage report.

## Translation

To make the update of the translation messages possible you need the xliffmerge tool. Add this tool to your enviroment with `ng add @ngx-i18nsupport/tooling --i18nLocale=en --languages de,en`. (You can set the default language to the one you use in your application by providing the respective language-code in the `--i18nLocale` flag and specify your supported languages with the `--languages` flag.)If you add text to the HTML-templates mark them with the `i18n`-attribute in the surrounding HTML-tag. [Use the official Angular Docs as reference for this step.](https://angular.io/guide/i18n). Now you can use the extraction script with `npm run extract-i18n` to update the `*.xlf` files in `src/locale` with your texts. This script is configured to run verbose. You will be notified that you need to translate some content, if there are no translations available. Do this manually in the respective `*.xlf` or use a [translation editor like poedit](https://poedit.net/). Use this [tutorial](https://github.com/martinroob/ngx-i18nsupport/wiki/Tutorial-for-using-xliffmerge-with-angular-cli) for further reading.

## Further Information

For further information regarding the usage of the Angular CLI, run `ng help` or refer to the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
