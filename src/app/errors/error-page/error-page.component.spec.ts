import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ErrorPageComponent} from './error-page.component';
import {Router} from '@angular/router';

describe('ErrorPageComponent', () => {
  let routerMock: RouterMock;
  let component: ErrorPageComponent;
  let fixture: ComponentFixture<ErrorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorPageComponent], providers: [{provide: Router, useClass: RouterMock}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorPageComponent);
    routerMock = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect on Button click to the homepage | admin', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('button');
    const spy = spyOn(routerMock, 'navigateByUrl');
    routerMock.url = '/admin';
    button.click();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalledWith('/admin/endpoints/management');

  });

  it('should redirect on Button click to the homepage | user', () => {
    const button: HTMLButtonElement = fixture.nativeElement.querySelector('button');
    const spy = spyOn(routerMock, 'navigateByUrl');
    routerMock.url = '/nothing-here';
    button.click();
    fixture.detectChanges();

    expect(spy).toHaveBeenCalledWith('/tasks/owned');
  });
});


class RouterMock {
  url: string;

  navigateByUrl(url: string) {
  }
}
