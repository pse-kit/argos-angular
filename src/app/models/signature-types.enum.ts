export enum SignatureTypes {
  CHECKBOX = 'checkbox',
  BUTTON = 'button',
  DOWNLOAD_BUTTON = 'buttonAfterDownload',
  DOWNLOAD_CHECKBOX = 'checkboxAfterDownload'
}
