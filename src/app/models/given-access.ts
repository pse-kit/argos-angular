import {User} from './user';
import {Group} from './group';

export interface GivenAccess {
  access_order: number;
  user?: User;
  group?: Group;
  user_id?: number;
  group_id?: number;
}
