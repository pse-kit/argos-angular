import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskOverviewComponent} from './task-overview.component';
import {ActivatedRoute, Router} from '@angular/router';
import {CacheMapService} from '../../../shared/cache-map.service';
import {of} from 'rxjs';
import {TaskDataService} from '../../../shared/task-data.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TaskOverviewComponent', () => {
  let component: TaskOverviewComponent;
  let fixture: ComponentFixture<TaskOverviewComponent>;
  let taskService: TaskDataService;
  let locale: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaskOverviewComponent],
      providers: [{provide: TaskDataService, useValue: taskService}, {provide: CacheMapService, useClass: FakeCache},
        {provide: Router, useClass: FakeRouter}, {provide: ActivatedRoute, useClass: FakeRoute}, {provide: locale, useValue: locale}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate flawlessly between pages', () => {
    component.nextPage();
    component.nextPage();
    expect(component.nextEnabled).toBe(false);
    component.previousPage();
    expect(component.nextEnabled).toBe(true);
  });

  it('should reload', () => {
    component.reload();
    expect(component).toBeTruthy();
  });
});

class FakeRouter {
  events = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of(new Event('test')))
  };

  navigate(path, params) {
    return true;
  };
}

class FakeRoute {
  queryParams = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({page: 1, entriesPerPage: 20}))
  };
  data = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({tasks: [{task_id: 1, name: 'test', endpoint_id: 1}]}))
  };
}

class FakeCache {

  flush(type: string) {
    return true;
  }
}
