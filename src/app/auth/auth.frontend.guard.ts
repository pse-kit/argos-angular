import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthorizationService} from './authorization.service';
import {UserDataService} from '../shared/user-data.service';

/**
 * Authorization Guard for the frontend module.
 * The Guard protects routes from unauthorized access.
 */
@Injectable({
  providedIn: 'root',
})
export class AuthFrontendGuard implements CanActivate {
  firstTime = true;

  constructor(private router: Router, private auth: AuthorizationService,
              private userDataService: UserDataService) {
  }

  /**
   * If the user ist not authorized, he will be redirected to /login, else he wil be redirected to
   * his desired url or /tasks/owned if no redirectUrl is provided.
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot, location = window.location): Observable<boolean> | Promise<boolean> | boolean {
    this.auth.setAdmin(false);

    // Login frontend
    if (state.url.startsWith('/login')) {
      if (this.auth.isAuthenticated() && !this.auth.isAdmin()) {

        // Load Language
        if (this.firstTime === true) {
          this.userDataService.getPreferences().subscribe(result => {
            localStorage.setItem('localeId', result.language);
            if (!location.pathname.includes('/' + result.language + '/')) {
              setTimeout(() => {
                location.replace('/' + result.language + '/tasks/owned');
              }, 1000);
            } else {
              this.router.navigateByUrl('/tasks/owned');
            }
          });
          this.firstTime = false;
        }
        return false;
      } else {
        return true;
      }
    }

    // Access control for frontend
    if (!this.auth.isAuthenticated() && !this.auth.isAdmin() && state.url.startsWith('/')) {
      if (!state.url.startsWith('/tasks/owned')) {
        localStorage.setItem('redirectUrl', state.url);
      }
      this.router.navigateByUrl('/login');
      return false;
    }

    if (this.firstTime === true) {
      this.userDataService.getPreferences().subscribe(result => {
        localStorage.setItem('localeId', result.language);
        if (!location.pathname.includes('/' + result.language + '/')) {
          setTimeout(() => {
            location.replace('/' + result.language + state.url);
          }, 1000);
        }
      });
    }
    this.firstTime = false;
    return true;
  }
}
