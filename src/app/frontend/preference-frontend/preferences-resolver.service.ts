import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Preference} from '../../models/preference';
import {UserDataService} from '../../shared/user-data.service';
import {EMPTY, Observable, of} from 'rxjs';
import {catchError, mergeMap, take} from 'rxjs/operators';

/**
 * Preferences Resolver
 */
@Injectable({
  providedIn: 'root'
})
export class PreferencesResolverService implements Resolve <Preference> {

  constructor(private userDataService: UserDataService, private router: Router) {
  }

  /**
   * Called when trying to access the preferences route. It will load the preferences settings from the webservice beforehand.
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Preference> | Promise<Preference> | Preference {
    return this.userDataService.getPreferences().pipe(
      take(1),
      mergeMap(preferences => {
        return of(preferences);
      }), catchError(err => {
        this.router.navigateByUrl(state.url);
        return EMPTY;
      })
    );
  }
}


