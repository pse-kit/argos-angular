import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {AuthorizationService} from './authorization.service';
import {AuthFrontendGuard} from './auth.frontend.guard';
import {UserDataService} from '../shared/user-data.service';
import {fakeAsync, tick} from '@angular/core/testing';
import {of} from 'rxjs';

describe('AuthFrontendGuard', () => {
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let authServiceSpy;
  const userDataSpy = jasmine.createSpyObj('UserDataService', ['getPreferences']);
  let authFrontendGuard: AuthFrontendGuard;

  // not used by AuthFrontendGuard
  const next: ActivatedRouteSnapshot = null;

  beforeEach(() => {
    routerSpy.navigateByUrl.calls.reset();
    authServiceSpy = jasmine.createSpyObj('AuthorizationService', ['setAdmin', 'isAuthenticated', 'isAdmin']);
    userDataSpy.getPreferences.calls.reset();
    authFrontendGuard = new AuthFrontendGuard(routerSpy as Router, authServiceSpy as AuthorizationService, userDataSpy as UserDataService);
    localStorage.clear();
  });

  it('Should set admin to false in the AuthorizationService', () => {
    const state = {url: '/login'};
    authFrontendGuard.canActivate(next, state as RouterStateSnapshot);

    expect(authServiceSpy.setAdmin).toHaveBeenCalledWith(false);
  });

  it('should let the user stay on the login page, if he is not authenticated', () => {
    const state = {url: '/login'};
    authServiceSpy.isAdmin.and.returnValue(false);
    authServiceSpy.isAuthenticated.and.returnValue(false);
    expect(authFrontendGuard.canActivate(next, state as RouterStateSnapshot)).toBeTruthy();
  });

  it('should navigate the user to the login page if he is not authenticated and setup the redirect url', () => {

    const state = {url: '/tasks/details/1'};
    authServiceSpy.isAdmin.and.returnValue(false);
    authServiceSpy.isAuthenticated.and.returnValue(false);
    expect(authFrontendGuard.canActivate(next, state as RouterStateSnapshot)).toBeFalsy();
    expect(localStorage.getItem('redirectUrl')).toBe('/tasks/details/1');
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/login');
  });

  it('should not redirect if actual language matches preference | on login page', fakeAsync(() => {
    const state = {url: '/login'};
    const spy = jasmine.createSpyObj('Location', ['replace']);
    spy.pathname = '/en/login';
    userDataSpy.getPreferences.and.returnValue(of({'language': 'en'}));
    authServiceSpy.isAdmin.and.returnValue(false);
    authServiceSpy.isAuthenticated.and.returnValue(true);
    authFrontendGuard.canActivate(next, state as RouterStateSnapshot, spy as Location);
    tick();
    expect(spy.replace).not.toHaveBeenCalled();
    expect(localStorage.getItem('localeId')).toBe('en');
  }));

  it('should redirect if actual language does not match preference | on login page', fakeAsync(() => {
    const state = {url: '/login'};
    const spy = jasmine.createSpyObj('Location', ['replace']);
    spy.pathname = '/en/login';
    userDataSpy.getPreferences.and.returnValue(of({'language': 'de'}));
    authServiceSpy.isAdmin.and.returnValue(false);
    authServiceSpy.isAuthenticated.and.returnValue(true);
    authFrontendGuard.canActivate(next, state as RouterStateSnapshot, spy as Location);
    tick(2000);
    expect(spy.replace).toHaveBeenCalledWith('/de/tasks/owned');
    expect(localStorage.getItem('localeId')).toBe('de');
  }));

  it('should not redirect if actual language matches preference | except login page', fakeAsync(() => {
    const state = {url: '/someUrl'};
    const spy = jasmine.createSpyObj('Location', ['replace']);
    spy.pathname = '/en/someUrl';
    userDataSpy.getPreferences.and.returnValue(of({'language': 'en'}));
    authServiceSpy.isAdmin.and.returnValue(false);
    authServiceSpy.isAuthenticated.and.returnValue(true);
    authFrontendGuard.canActivate(next, state as RouterStateSnapshot, spy as Location);
    tick();
    expect(spy.replace).not.toHaveBeenCalled();
    expect(localStorage.getItem('localeId')).toBe('en');
  }));

  it('should redirect if actual language does not match preference | except login page', fakeAsync(() => {
    const state = {url: '/someUrl'};
    const spy = jasmine.createSpyObj('Location', ['replace']);
    spy.pathname = '/en/someUrl';
    userDataSpy.getPreferences.and.returnValue(of({'language': 'de'}));
    authServiceSpy.isAdmin.and.returnValue(false);
    authServiceSpy.isAuthenticated.and.returnValue(true);
    authFrontendGuard.canActivate(next, state as RouterStateSnapshot, spy as Location);
    tick(2000);
    expect(spy.replace).toHaveBeenCalledWith('/de/someUrl');
    expect(localStorage.getItem('localeId')).toBe('de');
  }));
});
