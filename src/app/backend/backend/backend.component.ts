import {Component, OnInit} from '@angular/core';

/**
 * Container for the whole backend view.
 */
@Component({
  selector: 'app-backend',
  templateUrl: './backend.component.html',
  styleUrls: ['./backend.component.css']
})
export class BackendComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
