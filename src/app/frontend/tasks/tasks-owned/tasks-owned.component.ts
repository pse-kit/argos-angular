import {Component, Inject, LOCALE_ID} from '@angular/core';
import {TaskDataService} from '../../../shared/task-data.service';
import {CacheMapService} from '../../../shared/cache-map.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TaskOverviewComponent} from '../task-overview/task-overview.component';
import {TaskTypes} from '../../../models/task-types.enum';

@Component({
  selector: 'app-tasks-owned',
  templateUrl: './tasks-owned.component.html',
  styleUrls: ['./tasks-owned.component.css']
})
export class TasksOwnedComponent extends TaskOverviewComponent {

  constructor(protected taskDataService: TaskDataService, protected cache: CacheMapService, protected router: Router, protected activatedRoute: ActivatedRoute, @Inject(LOCALE_ID) private locale: string) {
    super(taskDataService, cache, router, activatedRoute);
    this.taskType = TaskTypes.OWNED;
  }

  transformDate(date: string): string {
    return new Date(date).toLocaleString();
  }

}
