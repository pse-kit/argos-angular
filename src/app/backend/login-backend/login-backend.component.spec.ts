import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import {LoginBackendComponent} from './login-backend.component';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthorizationService} from '../../auth/authorization.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModule} from '../../alert/alert.module';
import {asyncScheduler, Observable, of, throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {AlertMessage} from '../../alert/alert-messages';

describe('LoginBackendComponent', () => {
  let component: LoginBackendComponent;
  let fixture: ComponentFixture<LoginBackendComponent>;
  let fakeAuth: FakeAuthorizationService;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const routeSpy = {snapshot: jasmine.createSpyObj('ActivatedRoute', ['queryParams'])};
  let username: HTMLInputElement;
  let password: HTMLInputElement;
  let checkbox: HTMLSelectElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginBackendComponent],
      imports: [FormsModule, ReactiveFormsModule, AlertModule],
      providers: [{provide: AuthorizationService, useClass: FakeAuthorizationService},
        {provide: Router, useValue: routerSpy}, {provide: ActivatedRoute, useValue: routeSpy}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(LoginBackendComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      // Access to FakeAuthService
      fakeAuth = TestBed.get(AuthorizationService);

      // Setup DOM Elements for access
      username = fixture.nativeElement.querySelector('#inputUsername');
      password = fixture.nativeElement.querySelector('#inputPassword');
      checkbox = fixture.nativeElement.querySelector('#rememberMe');
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should show a fail message when providing wrong credentials', () => {
    // Set wrong credentials
    username.value = 'SomethingWrong';
    password.value = 'SomethingEvenWronger';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();
    // Login failed
    expect(component.message).toBe(AlertMessage.LOGIN_FAILED);

    // Check Message shows up
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#loginFailed')).not.toBeNull('Login failed message not shown');
  });

  it('should show a connectionError when simulating one', () => {
    // Set up a ConnectionError
    fakeAuth.connectionError = true;
    // Set wrong credentials
    username.value = 'SomethingWrong';
    password.value = 'SomethingEvenWronger';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();

    // Login failed
    expect(component.message).toBe(AlertMessage.CONNECTION_ERROR);

    // Check Message shows up
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#connectionError')).not.toBeNull('Connection error message not shown');
  });

  it('should navigate to /admin/endpoints/management when providing correct credentials', () => {
    // Set correct credentials
    username.value = 'testUser';
    password.value = 'testPassword';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();

    // args passed to router.navigateByUrl() spy
    const spy = routerSpy.navigateByUrl as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];

    // expecting navigation to admin/endpoints/management
    expect(navArgs).toBe('/admin/endpoints/management',
      'should navigate to endpoint management');
  });

  it('should disable the submit button, when login-request not completed', fakeAsync(() => {
    const submit: HTMLButtonElement = fixture.nativeElement.querySelector('#submitButton');
    // Set correct credentials
    username.value = 'testAsyncUser';
    password.value = 'testAsyncPassword';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();
    expect(submit.disabled).toBe(false);

    // Submit
    component.onSubmit();
    fixture.detectChanges();

    // Button should now be disabled
    expect(submit.disabled).toBe(true);

    // next value of observable
    tick();
    fixture.detectChanges();

    // After request button should be activated again
    expect(submit.disabled).toBe(false);
  }));

  it('should show ldap failed if, ldap failed', () => {
    // Set up a ConnectionError
    fakeAuth.connectionError = false;
    fakeAuth.ldapFailed = true;
    // Set wrong credentials
    username.value = 'SomethingWrong';
    password.value = 'SomethingEvenWronger';

    // create event so angular knows something changed these inputs
    username.dispatchEvent(new Event('input'));
    password.dispatchEvent(new Event('input'));

    // Update Bindings
    fixture.detectChanges();

    // Submit
    component.onSubmit();

    // Login failed
    expect(component.message).toBe(AlertMessage.LDAP_FAILED);

    // Check Message shows up
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#ldapFailed')).not.toBeNull('LdapFailed message not shown');
  });

  // TODO: Testing redirect on login

});


class FakeAuthorizationService {
  connectionError: Boolean = false;
  ldapFailed: Boolean = false;

  login(username: string, password: string, adminLogin: boolean, rememberMe: boolean) {
    if (this.connectionError === true) {
      return throwError(new HttpErrorResponse({status: 501}));
    } else {
      if (username === 'testUser' && password === 'testPassword' && adminLogin === true) {
        return new Observable(observer => observer.next(true));
      } else if (username === 'testAsyncUser' && password === 'testAsyncPassword' && adminLogin === true) {
        return of(true, asyncScheduler);
      } else {
        if (this.ldapFailed) {
          return throwError(new HttpErrorResponse({status: 500}));
        } else {
          return throwError(new HttpErrorResponse({status: 401}));
        }
      }
    }

  }
}
