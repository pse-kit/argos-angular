import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {BehaviorSubject, of} from 'rxjs';
import {FormArray, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {AlertMessage} from '../../../alert/alert-messages';
import {HttpResponse} from '@angular/common/http';
import {AlertModule} from 'src/app/alert/alert.module';
import {FlatpickrModule} from 'angularx-flatpickr';
import {NgbTooltipModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';

import {EndpointCreationComponent} from './endpoint-creation.component';
import {EndpointDataService} from 'src/app/shared/endpoint-data.service';
import {UserDataService} from 'src/app/shared/user-data.service';
import {Endpoint} from '../../../models/endpoint';

describe('EndpointCreationComponent', () => {
  let component: EndpointCreationComponent;
  let fixture: ComponentFixture<EndpointCreationComponent>;
  let endpointData: EndpointDataService;
  let userData: UserDataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EndpointCreationComponent],
      imports: [FormsModule, ReactiveFormsModule, AlertModule, NgbTooltipModule, NgbTypeaheadModule, FlatpickrModule.forRoot()],
      providers: [{provide: EndpointDataService, useClass: FakeEndpointService},
        {provide: UserDataService, useClass: FakeUserService}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(EndpointCreationComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      endpointData = TestBed.get(EndpointDataService);
      userData = TestBed.get(UserDataService);
    });
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('add signature', () => {
    let sig = {item: {email_address: 'mock@mocking.de', name: 'MockUser', user_id: 1}};
    component.addSignature(sig);
    sig = {item: {email_address: 'mocca@mocca.de', name: 'Cafe Mocca', user_id: 2}};
    component.addSignature(sig);
    expect(component.accesses).toEqual([{signee: {email_address: 'mock@mocking.de', name: 'MockUser', user_id: 1}, isUser: true},
      {signee: {email_address: 'mocca@mocca.de', name: 'Cafe Mocca', user_id: 2}, isUser: true}]);
  });

  it('remove access', () => {
    component.accesses = [{signee: {email_address: 'mock@mocking.de', name: 'MockUser', user_id: 1}, isUser: true},
      {signee: {email_address: 'mocca@mocca.de', name: 'Cafe Mocca', user_id: 2}, isUser: true}];
    component.removeAccess(0);
    expect(component.accesses).toEqual([{signee: {email_address: 'mocca@mocca.de', name: 'Cafe Mocca', user_id: 2}, isUser: true}]);
  });

  it('add form fields', () => {
    let expectedNumberOfFields = 4;
    component.addFormField();
    component.addFormField();
    component.addFormField();
    expect((<FormArray>component.endpointCreationForm.controls.fields).length).toBe(expectedNumberOfFields);
    for (let i = 0; i < expectedNumberOfFields; i++) {
      let field = (<FormArray>component.endpointCreationForm.controls.fields).at(i);
      expect(field.get('type').value).toBe('datetime');
      expect(field.get('value').value).toBe(null);
      expect(field.get('required').value).toBe(false);
    }
  });

  it('reset form', () => {
    component.message = AlertMessage.ENDPOINT_CREATED;

    component.addFormField();
    component.addFormField();
    component.addFormField();

    component.accesses = [{signee: {email_address: 'mock@mocking.de', name: 'MockUser', user_id: 1}, isUser: true},
      {signee: {email_address: 'mocca@mocca.de', name: 'Cafe Mocca', user_id: 2}, isUser: true}];

    component.endpointCreationForm.get('name').setValue('Some random name');
    component.endpointCreationForm.get('description').setValue('Some random description');
    component.endpointCreationForm.get('signatureType').setValue('buttonAfterDownload');
    component.endpointCreationForm.get('sequentialStatus').setValue(true);
    component.endpointCreationForm.get('deletableStatus').setValue(true);
    component.endpointCreationForm.get('emailNotificationStatus').setValue(true);
    component.endpointCreationForm.get('allowAdditionalAccessesStatus').setValue(true);
    component.endpointCreationForm.get('insertBefore').setValue(true);

    component.resetForm();

    expect(component.message.valueOf()).toBe(AlertMessage.NOT_SET);

    expect(component.endpointCreationForm.get('name').value).toBe(null);
    expect(component.endpointCreationForm.get('description').value).toBe(null);
    expect(component.endpointCreationForm.get('signatureType').value).toBe('checkbox');
    expect(component.endpointCreationForm.get('sequentialStatus').value).toBe(false);
    expect(component.endpointCreationForm.get('deletableStatus').value).toBe(false);
    expect(component.endpointCreationForm.get('emailNotificationStatus').value).toBe(false);
    expect(component.endpointCreationForm.get('allowAdditionalAccessesStatus').value).toBe(false);
    expect(component.endpointCreationForm.get('insertBefore').value).toBe(false);

    expect(component.accesses).toEqual([]);

    expect((<FormArray>component.endpointCreationForm.controls.fields).length).toBe(1);
    let field = (<FormArray>component.endpointCreationForm.controls.fields).at(0);
    expect(field.get('type').value).toBe('datetime');
    expect(field.get('value').value).toBe(null);
    expect(field.get('required').value).toBe(false);
  });

  it('remove field', () => {
    let firstField = new FormGroup({
      'type': new FormControl('textfield', Validators.required),
      'value': new FormControl('Some random text', Validators.required),
      'required': new FormControl(false)
    });
    let secondField = new FormGroup({
      'type': new FormControl('checkbox', Validators.required),
      'value': new FormControl(true, Validators.required),
      'required': new FormControl(false)
    });
    (<FormArray>component.endpointCreationForm.get('fields')).removeAt(0);
    (<FormArray>component.endpointCreationForm.get('fields')).push(firstField);
    (<FormArray>component.endpointCreationForm.get('fields')).push(secondField);

    component.removeField(0);

    expect((<FormArray>component.endpointCreationForm.controls.fields).length).toBe(1);
    let field = (<FormArray>component.endpointCreationForm.controls.fields).at(0);
    expect(field.get('type').value).toBe('checkbox');
    expect(field.get('value').value).toBe(true);
    expect(field.get('required').value).toBe(false);
  });

  it('check disable', () => {
    expect(component.checkDisable()).toBeTruthy();
  });

  it('submit endpoint', fakeAsync(() => {
    let endpoint: Endpoint = {
      'name': 'Some name',
      'description': 'Some description',
      'allow_additional_accesses_status': true,
      'deletable_status': false,
      'email_notification_status': true,
      'signature_type': 'checkbox',
      'sequential_status': false,
      'additional_access_insert_before_status': false,
      'fields': [{field_type: 'datetime', name: 'Some field name', required_status: true}],
      'given_accesses': []
    };

    component.endpointCreationForm.get('name').setValue('Some name');
    component.endpointCreationForm.get('description').setValue('Some description');
    component.endpointCreationForm.get('allowAdditionalAccessesStatus').setValue(true);
    component.endpointCreationForm.get('deletableStatus').setValue(false);
    component.endpointCreationForm.get('emailNotificationStatus').setValue(true);
    component.endpointCreationForm.get('signatureType').setValue('checkbox');
    component.endpointCreationForm.get('sequentialStatus').setValue(false);
    component.endpointCreationForm.get('insertBefore').setValue(false);

    let firstField = new FormGroup({
      'type': new FormControl('textfield', Validators.required),
      'value': new FormControl('Some random text', Validators.required),
      'required': new FormControl(false)
    });
    let secondField = new FormGroup({
      'type': new FormControl('checkbox', Validators.required),
      'value': new FormControl(true, Validators.required),
      'required': new FormControl(false)
    });
    (<FormArray>component.endpointCreationForm.get('fields')).removeAt(0);
    (<FormArray>component.endpointCreationForm.get('fields')).push(firstField);
    (<FormArray>component.endpointCreationForm.get('fields')).push(secondField);

    //component.endpointCreationForm.get('given_accesses')
    fixture.detectChanges();
    tick();

    component.onSubmit();

    expect(component.message).toBe(AlertMessage.ENDPOINT_CREATED);
  }));

  it('can deactivate', () => {
    expect(component.canDeactivate()).toBeTruthy();
  });
});


class FakeEndpointService {
  createEndpoint(endpoint: Endpoint) {
    return new BehaviorSubject(new HttpResponse({status: 200}));
  }
}

class FakeUserService {
  userSuggestions(searchTerm: string) {
    return of({user_id: 1, name: 'MockUser'});
  }

  groupSuggestions(searchTerm: string) {
    return of({group_id: 1, name: 'MockGroup'});
  }
}
