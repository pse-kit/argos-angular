import {User} from './user';
import {Field} from './field';
import {Access} from './access';

export interface Task {
  task_id?: number;
  name: string;
  creator?: User;
  task_status?: string;
  creation_date?: string;
  endpoint_id: number;
  signature_type?: string;
  sequential_status?: boolean;
  insert_before?: boolean;
  tasks_fields?: Field[];
  accesses?: Access[];
  comments?: Comment[];
  deletable?: boolean;
  can_sign?: boolean | string;
}
