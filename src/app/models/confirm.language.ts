export enum DE_Confirm {
  PROGRESS_LOST = 'Ihr momentaner Stand geht verloren. Wollen Sie fortfahren?',
  DELETE_ENDPOINT_ACTIVE_TASKS = 'Es sind aktive Aufträge für diesen Endpunkt vorhanden. Wollen Sie ihn als \"auslaufend\" markieren?',
  DELETE_ENDPOINT = 'Diesen Endpunkt löschen?',
  WITHDRAW_TASK = 'Diesen Auftrag zurückziehen?',
  REJECT_TASK = 'Diesen Auftrag ablehnen?'
}

export enum EN_Confirm {
  PROGRESS_LOST = 'Your current progress will be lost. Do you want to continue?',
  DELETE_ENDPOINT_ACTIVE_TASKS = 'There are active tasks for this endpoint. Do you want to mark it as \"deprecated\"?',
  DELETE_ENDPOINT = 'Delete this endpoint?',
  WITHDRAW_TASK = 'Withdraw this task?',
  REJECT_TASK = 'Reject this task?'
}
