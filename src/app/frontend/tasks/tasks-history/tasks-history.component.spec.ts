import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TasksHistoryComponent} from './tasks-history.component';
import {TaskOverviewComponent} from '../task-overview/task-overview.component';
import {ActivatedRoute, Router} from '@angular/router';
import {CacheMapService} from '../../../shared/cache-map.service';
import {TaskDataService} from '../../../shared/task-data.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TaskTypes} from '../../../models/task-types.enum';
import {of} from 'rxjs';

describe('TasksHistoryComponent', () => {
  let component: TasksHistoryComponent;
  let fixture: ComponentFixture<TasksHistoryComponent>;
  let cache: CacheMapService;
  let locale: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TasksHistoryComponent, TaskOverviewComponent],
      providers: [{provide: TaskDataService, useClass: FakeTaskService}, {provide: CacheMapService, useValue: cache},
        {provide: Router, useClass: FakeRouter}, {provide: ActivatedRoute, useClass: FakeRoute}, {provide: locale, useValue: locale}],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.taskType).toBe(TaskTypes.ARCHIVED);
  });

  it('should delete task', () => {
    component.deleteTask(1);
    expect(component.tasks.length).toBe(0);
  });
});

class FakeTaskService {

  deleteTask(id: number) {
    if (id === 1) {
      return of(true);
    } else {
      return of(false);
    }
  }
}

class FakeRouter {
  events = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of(new Event('test')))
  };
}

class FakeRoute {
  queryParams = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({page: 1, entriesPerPage: 20}))
  };
  data = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({tasks: [{task_id: 1, name: 'test', endpoint_id: 1}]}))
  };
}
