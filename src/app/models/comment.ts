import {User} from './user';

export interface Comment {
  comment_id: number;
  comment_date: string;
  creator: User;
  text: string;
}
