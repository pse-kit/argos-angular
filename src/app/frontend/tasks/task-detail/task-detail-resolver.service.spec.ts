import {TaskDetailResolverService} from './task-detail-resolver.service';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {TaskDataService} from '../../../shared/task-data.service';
import {fakeAsync, tick} from '@angular/core/testing';
import {Observable, of, throwError} from 'rxjs';

describe('TaskDetailResolverService', () => {

  const taskDataSpy = jasmine.createSpyObj('TaskDataService', ['getTaskDetails']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let taskDetailResolver: TaskDetailResolverService;

  beforeEach(() => {
    taskDataSpy.getTaskDetails.calls.reset();
    routerSpy.navigateByUrl.calls.reset();
    taskDetailResolver = new TaskDetailResolverService(taskDataSpy as TaskDataService, routerSpy as Router);
  });

  it('should create', () => {
    expect(taskDetailResolver).toBeTruthy();
  });

  it('should resolve tasks', fakeAsync(() => {
    const result = 'task';
    taskDataSpy.getTaskDetails.and.returnValue(of(result));
    const task = taskDetailResolver.resolve(new ActivatedRouteMock() as ActivatedRouteSnapshot, {url: '/task/details/1'} as RouterStateSnapshot);
    tick();
    expect(taskDataSpy.getTaskDetails).toHaveBeenCalledWith(1);
    (<Observable<any>>task).subscribe((returnValue) => expect(returnValue).toBe(result));
    tick();
  }));

  it('should navigate to /tasks/owned, if user in on /details and an error occured', fakeAsync(() => {
    taskDataSpy.getTaskDetails.and.returnValue(throwError(new Error('error')));
    tick();
    const task = taskDetailResolver.resolve(new ActivatedRouteMock() as ActivatedRouteSnapshot, {url: '/task/details/1'} as RouterStateSnapshot);
    (<Observable<any>>task).subscribe(() => fail());
    tick();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/tasks/owned');
  }));

  it('should navigate the user where he came from, if an error occurred', fakeAsync(() => {
    taskDataSpy.getTaskDetails.and.returnValue(throwError(new Error('error')));
    tick();
    const task = taskDetailResolver.resolve(new ActivatedRouteMock() as ActivatedRouteSnapshot, {url: '/tasks/history'} as RouterStateSnapshot);
    (<Observable<any>>task).subscribe(() => fail());
    tick();
    expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/tasks/history');
  }));
});

class ActivatedRouteMock {
  public paramMap = {
    get: function (key) {
      return '1';
    }
  };
}
