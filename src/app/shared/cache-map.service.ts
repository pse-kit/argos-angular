import {Injectable} from '@angular/core';
import {HttpRequest, HttpResponse} from '@angular/common/http';
import {Cache} from './cache';
import {CacheEntry} from './cache-entry';


@Injectable({
  providedIn: 'root'
})
export class CacheMapService implements Cache {

  cacheMap = new Map<string, CacheEntry>();

  constructor() {
  }

  get(req: HttpRequest<any>): HttpResponse<any> | null {
    const entry = this.cacheMap.get(req.urlWithParams);
    if (!entry) {
      return null;
    }
    return entry.response;
  }

  put(req: HttpRequest<any>, res: HttpResponse<any>): void {
    const entry: CacheEntry = {url: req.urlWithParams, response: res};
    this.cacheMap.set(req.urlWithParams, entry);
  }

  flush(type) {
    this.cacheMap.forEach(entry => {
      if (entry.url.includes(type)) {
        this.cacheMap.delete(entry.url);
      }
    });
  }
}
