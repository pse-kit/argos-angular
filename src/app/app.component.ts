import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titleSubscription: Subscription;

  constructor(private titleService: Title, private router: Router, private route: ActivatedRoute) {
    this.titleSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(event => this.createTitle());
  }

  createTitle() {
    let currentRoute = this.route.snapshot;
    while (currentRoute.firstChild !== null) {
      currentRoute = currentRoute.firstChild;
    }
    this.titleService.setTitle(currentRoute.data['breadcrumb']);
  }
}
