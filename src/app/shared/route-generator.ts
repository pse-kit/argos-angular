enum Routes {
  // Edit these two Routes
  ROOT_PATH = 'https://api.pse-kit.de',
  DOMAIN = 'pse-kit.de',
  API_VERSION = '/api/v1/',
  ENDPOINTS = 'endpoints/',
  TASKS = 'tasks/',
  STATUS = 'status/',
  COMMENTS = 'comments/',
  FILES = 'files/',
  PREFERENCES = 'preferences/',
  LANGUAGE = 'language/',
  NOTIFICATIONS = 'notifications/',
  USERS = 'users/',
  GROUPS = 'groups/',
  SEARCH = 'search/',
  CURRENT = 'current/',
  MESSAGES = 'messages/',
  REFRESH = 'refresh/',
  ACCESS = 'access/',
  LOGIN = 'login/',
  LOGOUT = 'logout/',
}

export class RouteGenerator {

  static getEndpointsRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.ENDPOINTS;
  }

  static getEndpointsIdRoute(id: number): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.ENDPOINTS + id;
  }

  static getTasksRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.TASKS;
  }

  static getTasksIdRoute(id: number): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.TASKS + id;
  }

  static getTasksStatusRoute(id: number): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.TASKS + id + '/' + Routes.STATUS;
  }

  static getTasksCommentsRoute(id: number): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.TASKS + id + '/' + Routes.COMMENTS;
  }

  static getTasksFilesRoute(id: number): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.TASKS + id + '/' + Routes.FILES;
  }

  static getPreferencesNotificationsRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.PREFERENCES + Routes.NOTIFICATIONS;
  }

  static getPreferencesLanguageRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.PREFERENCES + Routes.LANGUAGE;
  }

  static getUsersSearchRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.USERS + Routes.SEARCH;
  }

  static getGroupsSearchRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.GROUPS + Routes.SEARCH;
  }

  static getUsersCurrentRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.USERS + Routes.CURRENT;
  }

  static getMessagesRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.MESSAGES;
  }

  static getMessagesStatusRoute(id: number): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.MESSAGES + id + '/' + Routes.STATUS;
  }

  static getRefreshRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.REFRESH;
  }

  static getLoginRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.LOGIN;
  }

  static getLogoutAccessRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.LOGOUT + Routes.ACCESS;
  }

  static getLogoutRefreshRoute(): string {
    return Routes.ROOT_PATH + Routes.API_VERSION + Routes.LOGOUT + Routes.REFRESH;
  }

  static getDownloadRoute(url: string): string {
    return Routes.ROOT_PATH + url;
  }

  static getDomain(): string {
    return Routes.DOMAIN;
  }
}
