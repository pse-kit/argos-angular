import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EndpointManagementComponent} from './endpoint-management.component';
import {AlertModule} from '../../../alert/alert.module';
import {EndpointDataService} from '../../../shared/endpoint-data.service';
import {of, throwError} from 'rxjs';
import {CacheMapService} from '../../../shared/cache-map.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EndpointDetailComponent} from '../endpoint-detail/endpoint-detail.component';
import {AlertMessage} from '../../../alert/alert-messages';

describe('EndpointManagementComponent', () => {
  let component: EndpointManagementComponent;
  let fixture: ComponentFixture<EndpointManagementComponent>;
  let fakeEndpointService: FakeEndpointService;
  let fakeCache: FakeCache;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EndpointManagementComponent, EndpointDetailComponent],
      imports: [AlertModule],
      providers: [{provide: EndpointDataService, useClass: FakeEndpointService}, {provide: CacheMapService, useClass: FakeCache},
        {provide: Router, useValue: routerSpy}, {provide: ActivatedRoute, useClass: FakeRoute}]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(EndpointManagementComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      fakeEndpointService = TestBed.get(EndpointDataService);
      fakeCache = TestBed.get(CacheMapService);
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('should delete', () => {
    component.deleteEndpoint(1);
    expect(component).toBeTruthy();
  });

  it('should not find id to delete', () => {
    component.deleteEndpoint(2);
    expect(component.message).toBe(AlertMessage.SOMETHING_WENT_WRONG);
  });

  it('should navigate flawlessly between pages', () => {
    component.nextPage();
    component.nextPage();
    expect(component.nextEnabled).toBe(false);
    component.previousPage();
    expect(component.nextEnabled).toBe(true);
  });

  it('should reload after deletion', () => {
    component.reloadAfterDelete(1);
    expect(component.message).toBe(AlertMessage.NOT_SET);
  });

  it('should fail to reload after invalid deletion', () => {
    component.reloadAfterDelete(2);
    expect(component.message).toBe(AlertMessage.SOMETHING_WENT_WRONG);
  });
});

class FakeEndpointService {

  getEndpoints(page: number, entriesPerPage: number) {
    return of([{
      endpoint_id: 1, name: 'test', description: 'test', signature_type: 'checkbox', sequential_status: true,
      deletable_status: true, fields: [], instantiable_status: true, email_notification_status: true,
      allow_additional_accesses_status: true, given_accesses: [], active_tasks: 0, additional_access_insert_before_status: false
    }]);
  }

  deleteEndpoint(id: number) {
    if (id === 1) {
      return of(true);
    } else {
      return throwError(new Error());
    }
  }
}

class FakeCache {

  flush(type: string) {
    return true;
  }
}

class FakeRoute {
  queryParams = {
    subscribe: jasmine.createSpy('subscribe').and.returnValue(of({page: 1, entriesPerPage: 20}))
  };
}
